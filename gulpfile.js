const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

gulp.task('uglify', () => {
   gulp.src('wp-content/plugins/click-to-tweet-quote/js/**.js')
       .pipe(rename({ suffix: '.min' }))
       .pipe(uglify())
       .pipe(gulp.dest('wp-content/plugins/click-to-tweet-quote/js'))
})

gulp.task('default', () => {
   console.log('Gulp is running correctly!')
})