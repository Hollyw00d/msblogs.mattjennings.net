<?php
get_calendar();

echo '
	<style>
		#post-calendar-table {
			border: 1px solid black;
			border-collapse: collapse;
		}
		
		#post-calendar-table th,
		post-calendar-table td {
			border: 1px solid black;
		}
	</style>
';

$calender = '
	<table>
		<tr>
			<th>Sun</th>
			<th>Mon</th>
			<th>Tue</th>
			<th>Wed</th>
			<th>Thu</th>
			<th>Fri</th>
			<th>Sat</th>
		</tr>
';

$posts = new WP_Query(array(
        'no_found_rows'  => true,
        'posts_per_page' => -1
));

if ($posts->have_posts()): ?>
		<?php
		$prev_month = '';

		while ($posts->have_posts()): $posts->the_post();

				if (get_the_date('F Y') != $prev_month):
					$prev_month = get_the_date('F Y'); ?>

					<h2 class="month"><?= $prev_month; ?></h2>

				<?php endif; ?>

                <p><?php the_weekday(); ?>, <?php echo get_the_date('j'); ?>, <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>

		<?php endwhile; ?>

<?php endif; ?>