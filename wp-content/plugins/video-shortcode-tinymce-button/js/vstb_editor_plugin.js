// TinyMCE JS
(function() {
    tinymce.create('tinymce.plugins.vstb_button', {

        // url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            // In url replace '/js' with '/images' to get path to images
            var getImagesUrl = url.replace('/js', '/images');

            //add new button
            ed.addButton('vstb', {
                title : 'Video Shortcode',
                cmd : 'vstb_command',
                image : getImagesUrl + '/dashicons-editor-video-icon.png'
            });

            /*
            button functionality to wrap selected text ("video-source.mp4") with
            shortcode:
            [video src="video-source.mp4"]
            */
            ed.addCommand('vstb_command', function() {
                var selected_text = ed.selection.getContent();
                var return_text = '[video src="' + selected_text + '" poster=""]';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

        },
        // Button author info
        getInfo : function() {
            return {
                longname : 'Video Shortcode TinyMCE Button',
                author : 'Matt Jennings',
                version : '1'
            };
        }
    });

    tinymce.PluginManager.add('vstb_button', tinymce.plugins.vstb_button);
})();