=== Video Shortcode TinyMCE Button ===
Contributors: Matt Jennings
Tags: video
Tested up to: 4.9.6
License: GPLv2 or later

Video shortcode TinyMCE button allows you to add a TinyMCE button that when you click adds video shortcode for a video.

== Description ==

Video shortcode TinyMCE button allows you to add a TinyMCE button that when you click adds video shortcode for self-hosted WordPress video.

== Installation ==

Upload the Video Shortcode TinyMCE Button plugin to your blog and Activate it.

== Changelog ==
= 1.0.0 =
*Release Date - 2018-06-06*
