<?php
/*
Plugin Name: Video Shortcode TinyMCE Button
Plugin URI: https://www.mattjennings.net/
Description: Video shortcode TinyMCE button allows you to add a TinyMCE button that when you click adds video shortcode for self-hosted WordPress video.
Version: 1.0
Author: Matt Jennings
Author URI: : https://www.mattjennings.net/
License: GPL2
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: video-shortcode-tinymce-button
*/

class Video_Shortcode_TinyMCE_Button_MJ {
    private static $instance;

    public static function getInstance() {
        if(self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        // implement hooks here
        // Registers admin menu
        add_action('admin_menu', array($this, 'admin_menu'));


        // Hook to call TinyMCE filters
        add_action('init', array($this, 'custom_mce_button'));
    }

    // register custom plugin admin menu
    function admin_menu() {
        /* main menu */
        $top_menu_item = 'dashboard_admin_page';

        add_menu_page('', 'Video Shortcode TinyMCE Button', 'manage_options', $top_menu_item, $top_menu_item, 'dashicons-editor-video');
    }

    // dashboard admin page form
    function dashboard_admin_page() {
        ?>
        <div class="wrap">
        <h2><?php echo __('How to Use Video shortcode TinyMCE Button'); ?></h2>
        <p><?php echo __('Video shortcode TinyMCE button allows you to add a TinyMCE button that when you click adds video shortcode for a video.'); ?></p>
        <p><?php echo __('<strong>How to use:</strong>'); ?></p>
        <ol>
            <li><?php echo __('Add a new post in WordPress.'); ?></li>
            <li><?php echo __('Inside this post add a URL of a video, like <strong>http://example.com/video.mp4</strong>.'); ?></li>
            <li><?php echo __('Select the text by clicking your mouse button, holding it, and dragging it over the URL of the video, like <strong>http://example.com/video.mp4</strong>.'); ?></li>
            <li><?php echo __('Click the "Video Shortcode" TinyMCE button to wrap the video URL (<strong>http://example.com/video.mp4</strong>) in a video shortcode tag like<br /><strong>[video src="http://example.com/video.mp4" poster=""]</strong>.'); ?></li>
        </ol>
        <?php
    }

    // Enqueue TinyMCE JS
    function enqueue_plugin_scripts($plugin_array) {
        $plugin_array['vstb_button'] =  plugin_dir_url(__FILE__) . 'js/vstb_editor_plugin.js';
        return $plugin_array;
    }

    // Register TinyMCE button
    function register_buttons_editor($buttons) {
        array_push($buttons, 'vstb');
        return $buttons;
    }

    // Check if user has editor permissions and then call TinyMCE filters
    function custom_mce_button() {
        // Check if user have permission
        if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ):
            return;
        endif;
        if ( 'true' == get_user_option( 'rich_editing' ) ):
            add_filter('mce_external_plugins', array($this, 'enqueue_plugin_scripts'));
            add_filter('mce_buttons', array($this, 'register_buttons_editor'));
        endif;
    }

}

Video_Shortcode_TinyMCE_Button_MJ::getInstance();