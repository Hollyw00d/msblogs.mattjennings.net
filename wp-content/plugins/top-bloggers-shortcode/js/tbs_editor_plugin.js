// TinyMCE JS
(function() {
    tinymce.create('tinymce.plugins.tbs_button', {

        // url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            // In url replace '/js' with '/images' to get path to images
            var getImagesUrl = url.replace('/js', '/images');

            //add new button
            ed.addButton('tbs', {
                title : '',
                cmd : 'tbs_command',
                image : getImagesUrl + '/dashicons-chart-area-icon.png'
            });

            /*
            button functionality to add shortcode:
            [topbloggers count=""]
            */
            ed.addCommand('tbs_command', function() {
                var return_text = '[topbloggers count=""]';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

        },
        // Button author info
        getInfo : function() {
            return {
                longname : 'Top Bloggers Shortcode',
                author : 'Matt Jennings',
                version : '1'
            };
        }
    });

    tinymce.PluginManager.add('tbs_button', tinymce.plugins.tbs_button);
})();