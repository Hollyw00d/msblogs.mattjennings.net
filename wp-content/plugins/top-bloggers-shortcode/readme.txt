=== Top Bloggers Shortcode ===
Contributors: Matt Jennings
Tags: top bloggers, bloggers, blogger, top, shortcode
Tested up to: 4.9.6
License: GPLv2 or later

Top Bloggers Shortcode adds top bloggers shortcode (bloggers with the most posts) to a WordPress site.

== Description ==

Top Bloggers Shortcode adds top bloggers shortcode (bloggers with the most posts) to a WordPress site.

== Installation ==

Upload the Top Bloggers Shortcode plugin to your blog and Activate it.

== Changelog ==
= 1.0.0 =
*Release Date - 2018-06-22*
