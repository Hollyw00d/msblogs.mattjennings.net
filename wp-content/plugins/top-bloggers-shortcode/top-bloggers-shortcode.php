<?php
/*
Plugin Name: Top Bloggers Shortcode
Plugin URI: https://www.mattjennings.net/
Description: Top Bloggers Shortcode adds top bloggers shortcode (bloggers with the most posts) to a WordPress site.
Version: 1.0
Author: Matt Jennings
Author URI: : https://www.mattjennings.net/
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: top-blog-posts-shortcode
*/

class Top_Bloggers_Shortcode {
    private static $instance;

    public static function getInstance() {
        if(self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        // implement hooks here
        // Registers all custom shortcodes on init
        add_action('init', array($this, 'register_shortcodes'));

        // Registers admin menu
        add_action('admin_menu', array($this, 'admin_menu'));

        // Hook to call TinyMCE filters
        add_action('init', array($this, 'custom_mce_button'));

    }

    // Registers custom shortcodes
    function register_shortcodes() {
        add_shortcode('topbloggers', array($this, 'tbs_shortcode'));
    }

    // https://developer.wordpress.org/plugins/shortcodes/shortcodes-with-parameters/
    function tbs_shortcode( $attributes, $content = null ) {

        // Save ech attribute's values to its own variable.
        // Below are attribute values for hashtags and CSS styles
        extract( shortcode_atts( array(
            'count'           => ''
        ),  $attributes ) );

        // List authors 3 most popular authors by number of posts
        $authors_args = array(
            'orderby'           =>  'post_count',
            'order'             =>  'DESC'
        );

        $authors = get_users($authors_args);

        // setup output variable
        $output = '';

        $arr_count = 0;

        foreach($authors as $author):
            $author_job_title = get_field('author_job_title', 'user_' . $author->ID);

            if(count_user_posts($author->ID) > 0 && $arr_count < $count):
                $output .= '<div>'
                    . get_avatar($author->ID) .
                    '<h5>' . $author->display_name . '<br />' . $author_job_title . '</h5>' .
                    '</div>';
            endif;
            $arr_count++;
        endforeach;


        // return our results/html
        return $output;
    }

    // register custom plugin admin menu
    function admin_menu() {
        /* main menu */
        $top_menu_item = 'tbs_dashboard_admin_page';

        add_menu_page('', 'Top Bloggers Shortcode', 'manage_options', $top_menu_item, $top_menu_item, '
dashicons-chart-area');
    }

    // dashboard admin page form
    function tbs_dashboard_admin_page() {
        ?>
        <div class="wrap">
            <h2><?php echo __('Top Bloggers Shortcode Options Page'); ?></h2>
            <p><?php echo __('Top Bloggers Shortcode adds top bloggers shortcode (bloggers with the most posts) to a WordPress site.'); ?></p>
            <p><?php echo __('<strong>How to use:</strong>'); ?></p>
            <ol>
                <li><?php echo __('Add a new post in WordPress.'); ?></li>
                <li><?php echo __('Add in the shortcode below with <strong>count=3</strong> being teh number of popular authors that will appear:<br /><strong>[topbloggers count="3"]</strong>'); ?></li>
            </ol>
            <?php settings_errors(); ?>
        </div>
        <?php
    }

    // Enqueue TinyMCE JS
    function enqueue_plugin_scripts($plugin_array) {
        $plugin_array['tbs_button'] =  plugin_dir_url(__FILE__) . 'js/tbs_editor_plugin.js';
        return $plugin_array;
    }

    // Register TinyMCE button
    function register_buttons_editor($buttons) {
        array_push($buttons, 'tbs');
        return $buttons;
    }

    // Check if user has editor permissions and then call TinyMCE filters
    function custom_mce_button() {
        // Check if user have permission
        if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ):
            return;
        endif;
        if ( 'true' == get_user_option( 'rich_editing' ) ):
            add_filter('mce_external_plugins', array($this, 'enqueue_plugin_scripts'));
            add_filter('mce_buttons', array($this, 'register_buttons_editor'));
        endif;
    }
}

Top_Bloggers_Shortcode::getInstance();