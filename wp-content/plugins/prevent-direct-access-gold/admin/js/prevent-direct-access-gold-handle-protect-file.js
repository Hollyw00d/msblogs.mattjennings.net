(function( $ ) {
    'use strict';
    const { rest_api_prefix, home_url, nonce } = pda_gold_v3_data;
    const rootDomain = home_url.replace(/\/$/, '');
    const baseUrl = `${rootDomain}/${rest_api_prefix}/pda/v3`;

    $(function() {
        $(".pda-protect-file").click( function( evt ) {
            evt.preventDefault();
            const postId = evt.target.id.split('_')[1];
            pdaProtectFile(postId);
        });
    });

    function pdaProtectFile(postId) {
        let $pdaProtectFile = $("#pda-protect-file_" + postId);
        $pdaProtectFile.css("pointer-events", "none");
        var textProtect = $pdaProtectFile.text();
        $pdaProtectFile.text(textProtect + 'ing...');

        if ( textProtect.toLocaleLowerCase() === 'protect' ){
            callProtectFile(postId, textProtect);
        } else {
            callUnProtectFile(postId, textProtect);
        }
    }


    function callProtectFile( postId, textProtect ){
        const protectFileUrl =  `${baseUrl}/files/${postId}`;
        $.ajax({
            url: protectFileUrl,
            type: 'POST',
            timeout: 10000,
            headers: {
                'X-WP-Nonce': nonce
            }
        })
        .done((data, status, response) => {
            let $pdaProtectFile = $("#pda-protect-file_" + postId);
            if (response.status === 200) {
                toastr.success('Great! You\'ve successfully protected this file.', 'Prevent Direct Access Gold')
                $pdaProtectFile.text("Unprotect");
                $('#pda-v3-text_' + postId).remove();
                $('#pda-v3-wrap-status_' + postId).prepend('<span id="pda-v3-text_' + postId + '" class="protection-status " title="This file is protected"><i class="fa fa-check-circle" aria-hidden="true"></i> protected</span>');
            } else {
                toastr.error(data.message, 'Prevent Direct Access Gold');
                $pdaProtectFile.text(textProtect);
            }
            $pdaProtectFile.css("pointer-events", "auto");
        })
        .error(error => {
            toastr.error('Fail to protect the file. Request timeout.', 'Prevent Direct Access Gold');
        })
    }

    function callUnProtectFile( postId ){
        const unPortectFileUrl = `${baseUrl}/un-protect-files/${postId}`;
        $.ajax({
            url: unPortectFileUrl,
            type: 'POST',
            timeout: 10000,
            headers: {
                'X-WP-Nonce': nonce
            }
        })
        .done((data, status, response) => {
            let $pdaProtectFile = $("#pda-protect-file_" + postId);
            if (response.status === 200) {
                toastr.success('Great! You\'ve successfully unprotected this file.', 'Prevent Direct Access Gold')
                $pdaProtectFile.text("Protect");
                $('#pda-v3-text_' + postId).remove();
                $('#pda-v3-wrap-status_' + postId).prepend('<span id="pda-v3-text_' + postId + '" class="protection-status pda-unprotected" title="This file is unprotected"><i class="fa fa-times-circle" aria-hidden="true"></i> unprotected</span>');
            } else {
                toastr.error(data.message, 'Prevent Direct Access Gold');
                $pdaProtectFile.text(textProtect);
            }
            $pdaProtectFile.css("pointer-events", "auto");
        })
        .error(error => {
            toastr.error('Fail to unprotect the file. Request timeout.', 'Prevent Direct Access Gold');
         })
    }


})( jQuery );
