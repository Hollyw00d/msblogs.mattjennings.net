<?php

/**
 * Provide a admin area views for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://preventdirectaccess.com/extensions/?utm_source=user-website&utm_medium=pluginsite_link&utm_campaign=pda_gold
 * @since      1.0.0
 *
 * @package    Prevent_Direct_Access_Gold
 * @subpackage Prevent_Direct_Access_Gold/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
