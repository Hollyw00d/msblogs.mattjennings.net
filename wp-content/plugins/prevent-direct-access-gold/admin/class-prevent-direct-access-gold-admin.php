<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://preventdirectaccess.com/extensions/?utm_source=user-website&utm_medium=pluginsite_link&utm_campaign=pda_gold
 * @since      1.0.0
 *
 * @package    Prevent_Direct_Access_Gold
 * @subpackage Prevent_Direct_Access_Gold/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Prevent_Direct_Access_Gold
 * @subpackage Prevent_Direct_Access_Gold/admin
 * @author     BWPS <hello@preventdirectaccess.com>
 */
class Prevent_Direct_Access_Gold_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of this plugin.
	 * @param      string $version The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Prevent_Direct_Access_Gold_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Prevent_Direct_Access_Gold_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if ( function_exists( 'get_current_screen' ) ) {
			$screen          = get_current_screen();
			$support_screens = PDA_v3_Constants::get_screen_map_id();

			if ( $support_screens['media'] === $screen->id || $support_screens['attachment'] === $screen->id ) {
				wp_enqueue_style( 'pda-v3-metabox-css', plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-metabox.css', 'all', $this->version );
			}

			if ( $screen->id === $support_screens['affiliate'] ) {
				wp_enqueue_style( $this->plugin_name . 'affiliate', plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-affiliate.css', array(), $this->version, 'all' );
				wp_enqueue_style( $this->plugin_name . 'rating-subscibe', plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-rating-subscribe.css', array(), $this->version, 'all' );
			}

			if ( $screen->id === $support_screens['pda_settings']
			     || $screen->id === $support_screens['media']
			     || $screen->id === $support_screens['status'] ) {
				wp_enqueue_style( $this->plugin_name, plugin_dir_url( PDA_V3_PLUGIN_BASE_FILE ) . 'js/dist/style/pda_gold_v3_bundle.css', array(), $this->version, 'all' );
				wp_enqueue_style( $this->plugin_name . 'admin', plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-admin.css', array(), $this->version, 'all' );

				wp_enqueue_style( $this->plugin_name . 'rating-subscibe', plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-rating-subscribe.css', array(), $this->version, 'all' );

				wp_enqueue_style( $this->plugin_name . 'tagsinput', plugin_dir_url( __FILE__ ) . 'css/tagsinput.css', array(), $this->version, 'all' );
				wp_enqueue_style( $this->plugin_name . 'ui', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.min.css', array(), $this->version, 'all' );

				wp_enqueue_style( $this->plugin_name . 'pda_vs_toastr_css', 'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css', array(), $this->version, 'all' );
			}
			wp_enqueue_style(
				'select2_css',
				'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',
				null,
				'1.0'
			);
		}


		if ( isset( $_GET['tab'] ) && $_GET['tab'] === 'pda-quick-tour' ) {
			wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Gochi+Hand%7CKalam', false );

			wp_register_style( 'pda_v3_quick_tour_css' . $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-quick-tour.css', '1.0' );
			wp_enqueue_style( 'pda_v3_quick_tour_css' . $this->plugin_name );
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Prevent_Direct_Access_Gold_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Prevent_Direct_Access_Gold_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if ( isset( $_GET['tab'] ) && $_GET['tab'] === 'pda-quick-tour' ) {
			wp_enqueue_script( 'prevent-direct-access-gold-quick-tour-js' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/prevent-direct-access-gold-quick-tour.js', array(
				'jquery'
			), $this->version );
		}

		global $userAccessManager;
		if ( function_exists( 'get_current_screen' ) ) {
			$screen          = get_current_screen();
			$screen_map      = PDA_v3_Constants::get_screen_map_id();
			$helpers         = new Pda_Gold_Functions();
			$rest_api_prefix = $helpers->get_site_settings( PDA_v3_Constants::USE_REDIRECT_URLS ) ? '?rest_route=' : 'wp-json';
			if ( $screen_map['media'] === $screen->id ) {
				wp_enqueue_script( $this->plugin_name, plugin_dir_url( PDA_V3_PLUGIN_BASE_FILE ) . 'js/dist/pda_gold_v3_bundle.js', array( 'jquery' ), $this->version, false );
				wp_localize_script( $this->plugin_name, 'pda_gold_v3_data', array(
					'home_url'                             => $this->get_home_url_with_ssl(),
					'nonce'                                => wp_create_nonce( 'wp_rest' ),
					'stats_activated'                      => Yme_Plugin_Utils::is_plugin_activated( 'statistics' ),
					'magic_link_activated'                 => Yme_Plugin_Utils::is_plugin_activated( 'magic_link' ),
					'pda_s3_activated'                     => is_plugin_active( 'pda-s3/pda-s3.php' ) && apply_filters( PDA_Private_Hooks::PDA_HOOK_CHECK_S3_HAS_BUCKET, false ),
					'pda_membership_integration_activated' => Yme_Plugin_Utils::is_plugin_activated( 'membership' ),
					'user_access_manager'                  => is_object( $userAccessManager ),
					'memberships_2'                        => class_exists( 'MS_Model_Membership' ),
					'paid_memberships_pro'                 => function_exists( 'pmpro_hasMembershipLevel' ),
					'woo_memberships'                      => function_exists( 'wc_memberships_is_user_member' ),
					'woo_subscriptions'                    => class_exists( "WC_Subscriptions_Admin" ),
					'ar_member'                            => is_plugin_active( 'armember/armember.php' ),
					'pda_v3_plugin_url'                    => PDA_BASE_URL,
					'rest_api_prefix'                      => $rest_api_prefix,
					'is_license_expired'                   => Pda_Gold_Functions::is_license_expired()
				) );
			}

			if ( $screen_map['media'] === $screen->id || $screen_map['attachment'] === $screen->id ) {
				wp_enqueue_script( 'metabox' . $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/prevent-direct-acess-gold-metabox.js', array( 'jquery' ), $this->version );
				wp_localize_script( 'metabox' . $this->plugin_name, 'pda_gold_v3_metabox', array(
					'home_url'        => $this->get_home_url_with_ssl(),
					'nonce'           => wp_create_nonce( 'wp_rest' ),
					'rest_api_prefix' => $rest_api_prefix,
				) );
			}

			if ( $screen_map['pda_settings'] === $screen->id || $screen_map['upload'] === $screen->id || $screen_map['attachment'] === $screen->id ) {
				$check_install_ip_block       = Yme_Plugin_Utils::is_plugin_activated( 'ip_block' );
				$check_install_pda_magic_link = Yme_Plugin_Utils::is_plugin_activated( 'magic_link' );

				$roles     = get_editable_roles();
				$userRoles = [];
				foreach ( $roles as $roleName => $roleValue ) {
					array_push( $userRoles, $roleName );
				}

				wp_enqueue_script( 'ip_block_js' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/pdav3-ip-block.js', array(), $this->version );
				wp_localize_script( 'ip_block_js' . $this->plugin_name, 'ip_block_server_data', array(
					'ajaxurl'                  => admin_url( 'admin-ajax.php' ),
					'ip_block_activated'       => $check_install_ip_block,
					'pda_magic_link_activated' => $check_install_pda_magic_link,
					'roles'                    => $userRoles,
				) );
			}

			if ( is_admin() ) {
				wp_enqueue_script( 'pda-add-class-to-media-library-grid-elements', plugin_dir_url( __FILE__ ) . './js/decorate_grid_view.js', array( 'jquery' ), $this->version ); //Edit to match the file location
				wp_enqueue_style( 'pda-add-class-to-media-library-grid-elements', plugin_dir_url( __FILE__ ) . './css/decorate_grid_view.css', 'all', $this->version );
			}
		}

		wp_enqueue_script( 'pda_s3_toastr_' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/lib/toastr.min.js', $this->version );

		wp_enqueue_script( 'select2_js' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/lib/select2.min.js', array( 'jquery' ), '4.0.3', true );

		if ( ! wp_script_is( 'jquery_ui_min' ) ) {
			wp_enqueue_script( 'jquery_ui_min' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/jquery-ui.min.js', array( 'jquery' ), $this->version );
		}
		if ( ! wp_script_is( 'jquery_tagsinput_min' ) ) {
			wp_enqueue_script( 'jquery_tagsinput_min' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/jquery.tagsinput.min.js', array( 'jquery' ), $this->version );
		}

	}

	function pda_v3_attachment_edit_fields_styles_and_scripts() {
		$helpers         = new Pda_Gold_Functions();
		$rest_api_prefix = $helpers->get_site_settings( PDA_v3_Constants::USE_REDIRECT_URLS ) ? '?rest_route=' : 'wp-json';
		wp_enqueue_script( 'metabox' . $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/prevent-direct-acess-gold-metabox.js', array( 'jquery' ), $this->version );
		wp_localize_script( 'metabox' . $this->plugin_name, 'pda_gold_v3_metabox', array(
			'home_url'        => $this->get_home_url_with_ssl(),
			'nonce'           => wp_create_nonce( 'wp_rest' ),
			'rest_api_prefix' => $rest_api_prefix,
		) );
		wp_enqueue_style( 'metabox-css' . $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/prevent-direct-access-gold-metabox.css', array( 'media-editor' ), $this->version );
	}

	private function get_home_url_with_ssl() {
		return is_ssl() ? home_url( '/', 'https' ) : home_url( '/' );
	}


	public function pda_rest_api_init_cb() {
		$api = new PDA_Api_Gold;
		$api->register_rest_routes();
	}

	function pda_add_upload_columns( $columns ) {
		$columns[ PDA_v3_Constants::COLUMN_ID ] = '<label>' . __( "Prevent Direct Access", "prevent-direct-access-gold" ) . '</label>';

		return $columns;
	}

	function pda_media_custom_column( $column_name, $post_id ) {
		if ( $column_name == PDA_v3_Constants::COLUMN_ID ) {
			PDA_ViewLoader::render_custom_column( $post_id );
		}
	}

	public function pda_db_handle() {
		$db = new PDA_v3_DB;
		$db->run();
	}

	public function Prevent_Direct_Access_Gold_create_plugin_menu() {

		add_submenu_page( 'pda-gold', __( 'Settings', 'prevent-direct-access-gold' ), __( 'Settings', 'prevent-direct-access-gold' ), 'manage_options', 'pda-gold', '', 'dashicons-hidden' );

		require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/class-prevent-direct-access-gold-settings.php';
		$setting_page   = new Prevent_Direct_Access_Gold_Settings();
		$menu_page_name = add_menu_page( 'Prevent Direct Access Gold Settings', 'Prevent Direct Access Gold', 'administrator', PDA_v3_Constants::SETTING_PAGE_PREFIX, array(
			$setting_page,
			'render_ui'
		), PDA_BASE_URL . 'public/assets/pda-logo-20px.png' );

		$this->pda_add_status_submenu();
		$this->pda_add_affiliate_submenu();

		add_action( 'admin_print_styles-' . $menu_page_name, array(
			$this,
			'prevent_direct_access_gold_setting_assets'
		) );

	}

	public function add_body_classes_for_quick_tour( $classes ) {
		$is_quick_tour = isset( $_GET['tab'] ) && $_GET['tab'] === "pda-quick-tour" ? true : false;
		if ( $is_quick_tour ) {
			return $classes . ' toplevel_page_pda-wthrou-guide';
		}

		return $classes;
	}

	public function pda_add_status_submenu() {
		$setting_status = new PDA_Status;

		add_submenu_page( 'pda-gold', __( 'Status', 'prevent-direct-access-gold' ), __( 'Status', 'prevent-direct-access-gold' ), 'manage_options', PDA_v3_Constants::STATUS_PAGE_PREFIX, array(
			$setting_status,
			'render_ui'
		), 'dashicons-hidden' );
	}

	public function pda_add_affiliate_submenu() {
		$setting_affiliate = new PDA_Affiliate;

		add_submenu_page( 'pda-gold', __( 'Invite & Earn', 'prevent-direct-access-gold' ), __( 'Invite & Earn', 'prevent-direct-access-gold' ), 'manage_options', PDA_v3_Constants::AFFILIATE_PAGE_PREFIX, array(
			$setting_affiliate,
			'render_ui'
		), 'dashicons-hidden' );
	}

	function prevent_direct_access_gold_setting_assets() {
		wp_register_style( 'pda_v3_setting_' . $this->plugin_name, plugin_dir_url( PDA_V3_PLUGIN_BASE_FILE ) . '/admin/css/prevent-direct-access-gold-setting.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'pda_v3_setting_' . $this->plugin_name );
		wp_enqueue_script( 'prevent-direct-access-gold-setting-general' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/prevent-direct-access-gold-setting-general.js', array(
			'jquery'
		), $this->version );

		wp_localize_script( 'prevent-direct-access-gold-setting-general' . $this->plugin_name, 'prevent_direct_access_gold_setting_data',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'home_url' => $this->get_home_url_with_ssl()
			)
		);
		wp_enqueue_script( 'prevent-direct-access-gold-license' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/prevent-direct-access-gold-license.js', array( 'jquery' ), $this->version );
		wp_localize_script( 'prevent-direct-access-gold-license' . $this->plugin_name, 'prevent_direct_access_gold_license_data', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'search_gold_js' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/pdav3_setting_search.js', array( 'jquery' ), $this->version );
		wp_localize_script( 'search_gold_js' . $this->plugin_name, 'server_data', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_enqueue_script( 'prevent-direct-access-gold-newsletter' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/prevent-direct-access-gold-newsletter.js', array( 'jquery' ), $this->version );
		wp_localize_script(
			'prevent-direct-access-gold-newsletter' . $this->plugin_name,
			'newsletter_data',
			array(
				'newsletter_url'   => admin_url( 'admin-ajax.php' ),
				'newsletter_nonce' => wp_create_nonce( 'pda_gold_subscribe' )
			)
		);

		if ( is_multisite() && get_current_blog_id() === 1 ) {
			wp_enqueue_script( 'prevent-direct-access-gold-setting-general-license-multisite' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/prevent-direct-access-gold-setting-general-license-multisite.js', array(
				'jquery'
			), $this->version );
		}

	}

	public function Prevent_Direct_Access_Gold_Check_Licensed() {
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, PDA_v3_Constants::LICENSE_FORM_NONCE ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		$license = $_REQUEST['license'];

		if ( empty( $license ) ) {
			update_option( PDA_v3_Constants::LICENSE_OPTIONS, false, 'no' );
			update_option( PDA_v3_Constants::LICENSE_ERROR, 'Invalid license', 'no' );
			update_option( PDA_v3_Constants::LICENSE_KEY, '', 'no' );
			wp_send_json( false );
		}

		$result = YME_LICENSE::checkLicense( $license, 'pda', get_site_option( PDA_v3_Constants::APP_ID ) );
		$data   = $result['data'];

		$logger = new PDA_Logger;
		$logger->info( sprintf( "License checking response: %s", json_encode( $data ) ) );

		if ( ! $data ) {
			$result['data']['errorMessage'] = "There is something's wrong. Please <a href=\"hello@preventdirectaccess.com\">contact</a> the plugin owner!";
			update_option( PDA_v3_Constants::LICENSE_OPTIONS, false, 'no' );
			update_option( PDA_v3_Constants::LICENSE_ERROR, $data['errorMessage'], 'no' );
			update_option( PDA_v3_Constants::LICENSE_KEY, '', 'no' );
		} else if ( is_object( $data ) && property_exists( $data, 'errorMessage' ) ) {
			update_option( PDA_v3_Constants::LICENSE_OPTIONS, false, 'no' );
			update_option( PDA_v3_Constants::LICENSE_ERROR, $data->errorMessage, 'no' );
			update_option( PDA_v3_Constants::LICENSE_KEY, '', 'no' );
		} else {
			update_option( PDA_v3_Constants::LICENSE_KEY, $license, 'no' );
			update_option( PDA_v3_Constants::LICENSE_OPTIONS, true, 'no' );
			update_option( PDA_v3_Constants::LICENSE_ERROR, '', 'no' );
			delete_option( PDA_v3_Constants::LICENSE_EXPIRED );

			$service = new PDA_Services();
			$service->handle_license_info();
			$cronjob_handler = new PDA_Cronjob_Handler();
			$cronjob_handler->schedule_ls_cron_job();

			$db = new PDA_v3_DB;
			$db->run();
		}
		wp_send_json( $result );
		wp_die();
	}

	function Prevent_Direct_Access_Gold_admin_notices() {
		if ( ! current_user_can( 'install_plugins' ) ) {
			return;
		}

		if ( function_exists( 'get_current_screen' ) ) {
			$screen = get_current_screen();

			if ( ! in_array( $screen->id, array(
				'plugins-network',
				'plugins',
				'options-media',
				'upload',
				'media',
				'attachment',
				'toplevel_page_pda-gold'
			) ) ) {
				return;
			}

			$is_licensed = get_option( PDA_v3_Constants::LICENSE_OPTIONS );
			if ( ! Pda_v3_Gold_Helper::is_migrated_data_from_v2() ) {
				$this->notice_migrate_data();
			} else if ( is_null( $is_licensed ) || ! $is_licensed || empty( $is_licensed ) ) {
				?>
				<div class="error is-dismissible notice">
					<p>
						<b><?php echo "Prevent Direct Access Gold: "; ?></b> <?php echo esc_html__( 'Please enter your license key that you’ve', 'prevent-direct-access-gold' ) ?>
						<?php echo esc_html__( 'received after purchasing our plugin under', 'prevent-direct-access-gold' ) ?>
						<b>Prevent Direct Access
							Gold</b> <?php echo esc_html__( 'tab. Otherwise, the plugin\'s premium features are not activated.', 'prevent-direct-access-gold' ) ?>
					</p>
				</div>
				<?php
			} else if ( is_plugin_active( 'json-rest-api/plugin.php' ) ) {
				//plugin is activated
				?>
				<div class="error is-dismissible notice">
					<p>
						<b><?php echo "Prevent Direct Access: "; ?></b><?php echo esc_html__( 'You are using WP REST API which is deprecated.', 'prevent-direct-access-gold' ) ?>
						<?php echo esc_html__( 'Please update to', 'prevent-direct-access-gold' ) ?> <a
								href="https://wordpress.org/plugins/rest-api/"><?php echo esc_html__( 'WordPress REST API (Version 2)', 'prevent-direct-access-gold' ) ?></a>
					</p>
				</div>
				<?php
			} else if ( ! Pda_Gold_Functions::is_fully_activated() ) {
				$this->notice_htaccess();
			} else if ( Pda_Gold_Functions::is_license_expired() ) {
				$message = PDA_v3_Constants::LICENSE_EXPIRED_MESSAGE;
				?>
				<div class="error is-dismissible notice">
					<p><?php echo html_entity_decode( $message ) ?> </p>
				</div>
				<?php
			}

			$version        = explode( '.', PHP_VERSION );
			$php_version_id = $version[0] * 10000 + $version[1] * 100;
			if ( $php_version_id < 50500 ) {
				?>
				<div class="error is-dismissible notice">
					<p><b><?php echo "Prevent Direct Access Gold: "; ?></b>
						<?php echo esc_html__( 'You\'re using an outdated version of PHP which is not compatible with our plugin. Please upgrade to PHP version 5.5 or greater.', 'prevent-direct-access-gold' ) ?>
						<br>
						<?php echo esc_html__( 'Outdated PHP or MySQL versions may also expose your site to security vulnerabilities.', 'prevent-direct-access-gold' ) ?>
					</p>
				</div>
				<?php
			}
			$function = new Pda_Gold_Functions();
			if ( $function->is_move_files_after_activate_async() && ! empty( get_option( PDA_v3_Constants::PDA_NOTICE_CRONJOB_AFTER_ACTIVATE_OPTION ) ) ) {
				$status_files = $function->get_status_move_files();
				?>
				<div class="error is-dismissible notice">
					<p><b><?php echo "Prevent Direct Access Gold: "; ?></b>
						<?php echo esc_html__( 'We’re handling ' . $status_files['num_of_protected_files'] . '/' . $status_files['total_files'] . ' protected files. Please come back in a while.', 'prevent-direct-access-gold' ) ?>
					</p>
				</div>
				<?php
			}

			$updates = Pda_v3_Gold_Helper::extensions_has_updates();
			if ( ! empty( $updates ) ) {
				if ( count( $updates ) > 1 ) {
					$last = $updates[ count( $updates ) - 1 ];
					$tmp  = array_slice( $updates, 0, count( $updates ) - 1 );
					$text = implode( ', ', $tmp );
					$text .= " and $last";
				} else {
					$text = implode( ', ', $updates );
				}
				?>
				<div class="notice notice-warning is-dismissible">
					<p>
						<b><?php echo "Prevent Direct Access Gold: "; ?></b> Please upgrade <?php echo $text ?>
						extensions to their latest versions to enjoy our new awesome features.
					</p>
				</div>
				<?php
			}
		}
	}

	private function notice_htaccess() {
		$helper_url = network_admin_url( 'admin.php?page=pda-gold' );
		?>
		<div class="error is-dismissible notice">
			<p><b><?php echo "Prevent Direct Access Gold: "; ?></b> Almost there. Our Gold version requires you to
				insert some simple .htaccess rewrite rules into your .htaccess file for our plugin to work properly.
				Please follow this <a href="<?php echo $helper_url ?>">instructions</a> on how to do it.</p>
		</div>
		<?php
	}

	private function notice_migrate_data() {
		$setting_url = network_admin_url( 'admin.php?page=pda-gold' );
		?>
		<div class="notice notice-success is-dismissible">
			<p><b><?php echo "Prevent Direct Access Gold: "; ?></b> Congratulations! Prevent Direct Access Gold has
				been updated to version 3.0. Please 1) deactivate and activate the plugin now then 2) migrate your
				data under <a href="<?php echo $setting_url ?>">our Settings page</a> to the latest version.</p>
		</div>
		<?php
	}

	function save_product_id_to_option() {

		if ( get_site_option( PDA_v3_Constants::APP_ID ) === false ) {
			$configs = include( PDA_V3_BASE_DIR . 'includes/class-prevent-direct-access-gold-configs.php' );
			update_site_option( PDA_v3_Constants::APP_ID, $configs->app_id );
		}

		if ( get_option( PDA_v3_Constants::MIGRATE_DATA, null ) === null ) {
			add_option( PDA_v3_Constants::MIGRATE_DATA, true, '', 'no' );
		}
	}

	public function pda_gold_update_general_settings() {
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, 'pda_ajax_nonce_v3' ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		$settings = $_REQUEST['settings'];

		$keys                = [];
		$group_htaccess_keys = [];

		foreach ( $settings as $key => $value ) {
			if ( $key === PDA_v3_Constants::PDA_PREVENT_ACCESS_LICENSE
			     || $key === PDA_v3_Constants::PDA_GOLD_ENABLE_IMAGE_HOT_LINKING
			     || $key === PDA_v3_Constants::PDA_GOLD_ENABLE_DERECTORY_LISTING
			     || $key === PDA_v3_Constants::PDA_PREFIX_URL
			     || $key === PDA_v3_Constants::REMOVE_LICENSE_AND_ALL_DATA
			     || $key === PDA_v3_Constants::USE_REDIRECT_URLS ) {
				array_push( $group_htaccess_keys, $key );
			} else {
				array_push( $keys, $key );
			}
		}

		$options_v3_db = unserialize( get_option( PDA_v3_Constants::OPTION_NAME ) );
		foreach ( $keys as $k ) {
			$options_v3_db[ $k ] = $settings[ $k ];
		}
		$site_options_v3_db = unserialize( get_site_option( PDA_v3_Constants::SITE_OPTION_NAME ) );
		foreach ( $group_htaccess_keys as $k ) {
			$site_options_v3_db[ $k ] = $settings[ $k ];
		}

		$pda_v3      = serialize( $options_v3_db );
		$pda_v3_site = serialize( $site_options_v3_db );

		update_option( PDA_v3_Constants::OPTION_NAME, $pda_v3, 'no' );
		update_site_option( PDA_v3_Constants::SITE_OPTION_NAME, $pda_v3_site );
		Prevent_Direct_Access_Gold_Htaccess::register_rewrite_rules();
		wp_send_json( $settings );
		wp_die();
	}

	function pda_custom_upload_filter( $metadata, $attachment_id ) {
		$pda_gold_functions = new Pda_Gold_Functions();
		$roles              = $pda_gold_functions->selected_roles( PDA_v3_Constants::WHITElIST_ROLES_AUTO_PROTECT );
		$current_role       = Pda_v3_Gold_Helper::get_current_role();
		if ( isset( $_POST['is_upload_from_media'] ) ) {
			// Upload media in media-new.php
			// If checkbox is uncheck
			if ( ! isset( $_POST['pda_protect_media_upload'] ) ) {
				return $metadata;
			} else if ( $_POST['pda_protect_media_upload'] != 'on' ) {
				return $metadata;
			}

		} else {
			// Upload media in post.php
			$auto_protect_new_file = $pda_gold_functions->getSettings( PDA_v3_Constants::PDA_AUTO_PROTECT_NEW_FILE );
			if ( ! $auto_protect_new_file ) {
				return $metadata;
			}

			if ( ! empty( $roles ) && empty( array_intersect( $current_role, $roles ) ) ) {
				return $metadata;
			}
		}

		return $this->pda_protect_file( $metadata, $attachment_id );
	}

	public function so_wp_ajax_update_ip_block() {
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, 'pda_ajax_nonce_ip_block' ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		$settings = $_REQUEST['settings'];
		update_option( 'pda_gold_ip_block', $settings['pda_gold_ip_block'], 'no' );
		Prevent_Direct_Access_Gold_Htaccess::register_rewrite_rules();
		wp_send_json( true );
		wp_die();
	}

	public function pda_ajax_pda_gold_subscribe() {
		$check = check_ajax_referer( 'pda_gold_subscribe', 'security_check' );
		if ( $check == 1 ) {
			if ( $_POST['action'] == 'pda_gold_subscribe' ) {
				$data     = array(
					'email'    => $_POST['email'],
					'campaign' => array(
						'campaignId' => 'atMwe',
					)
				);
				error_log( 'DEBUG: ' . wp_json_encode( $data ) );
				$args     = array(
					'body'        => json_encode( $data ),
					'timeout'     => '100',
					'redirection' => '5',
					'httpversion' => '1.0',
					'blocking'    => true,
					'headers'     => array(
						'X-Auth-Token' => 'api-key ae824cfc3df1a2aa18e8a5419ec1c38b',
						'Content-Type' => 'application/json'
					),
				);
				$response = wp_remote_post(
					'https://api.getresponse.com/v3/contacts',
					$args
				);
				if ( is_wp_error( $response ) ) {
					$result['message'] = $response->get_error_message();
				} else {
					$result['data'] = json_decode(wp_remote_retrieve_body($response));
					$uid = get_current_user_id();
					update_user_meta( $uid, 'pda_gold_subscribe', true );
				}
				return $result;
			}
		}
	}

	public function pda_v3_custom_robots_txt( $output ) {
		$upload      = wp_upload_dir();
		$upload_path = str_replace( home_url( '/' ), '', $upload['baseurl'] );
		$site_url    = parse_url( site_url() );
		$path        = ( ! empty( $site_url['path'] ) ) ? $site_url['path'] : '';
		$rules       = "Disallow: $path/" . $upload_path . "/_pda/*" . PHP_EOL;

		return $output . $rules;
	}

	public function add_no_index_meta() {
		global $post;

		if ( isset( $post ) ) {
			$repo = new PDA_v3_Gold_Repository;
			if ( $repo->is_protected_file( $post->ID ) ) {
				?>
				<meta name="robots" content="noindex">
				<?php
			}
		}
	}

	function add_custom_setting_metabox() {
		$gold_function = new Pda_Gold_Functions();
		if ( $gold_function->pda_check_role_protection() ) {
			include( PDA_V3_BASE_DIR . 'includes/class-prevent-direct-access-gold-metabox.php' );
		}
	}

	function pda_gold_migrate_data() {
		$repo  = new PDA_v3_Gold_Repository();
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, 'pda_ajax_nonce_v3' ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		$attachment_ids = $repo->get_protected_posts();
		$names          = array();
		foreach ( $attachment_ids as $id ) {
			$file = get_post_meta( $id['post_id'], '_wp_attached_file', true );
			if ( ! empty( $file ) ) {
				$info = pathinfo( $file );
				if ( false === stripos( $file, Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir( '/' ) ) ) {
					$reldir = dirname( $file );
					if ( in_array( $reldir, array( '\\', '/', '.' ), true ) ) {
						$reldir = '';
					}
					$protected_dir = path_join( Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir(), $reldir );
					$move_result   = Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected( $id['post_id'], $protected_dir );
					if ( ! is_wp_error( $move_result ) ) {
						error_log( "Moved $file successfully. Updating post meta: " . $id['post_id'] );
						update_post_meta( $id['post_id'], PDA_v3_Constants::PROTECTION_META_DATA, true );
					}
					array_push( $names,
						array(
							'basename' => $info['basename'],
							'id'       => $id['post_id']
						)
					);
				}
			}
		}

		$repo->migrate_pda_options();

		update_option( PDA_v3_Constants::MIGRATE_DATA, true, 'no' );
		Prevent_Direct_Access_Gold_Htaccess::register_rewrite_rules();

		return $names;
	}

	function pda_custom_bulk_actions( $actions ) {
		$gold_function = new Pda_Gold_Functions();
		if ( $gold_function->pda_check_role_protection() ) {
			$actions['pda_v3_prevent_all_files']    = __( 'Protect files', 'pda_v3_prevent_all_files' );
			$actions['pda_v3_un_prevent_all_files'] = __( 'Unprotect files', 'pda_v3_un_prevent_all_files' );
		}

		return $actions;
	}

	function pda_bulk_action_handler( $redirect_to, $doaction, $post_ids ) {
		if ( $doaction !== 'pda_v3_prevent_all_files' && $doaction !== 'pda_v3_un_prevent_all_files' ) {
			return $redirect_to;
		}
		if ( isset( $post_ids ) ) {
			$doaction == 'pda_v3_prevent_all_files' ? true : false;
			if ( $doaction == 'pda_v3_prevent_all_files' ) {
				foreach ( $post_ids as $post_id ) {
					$this->protect_prevent_files( $post_id );
				}
			} else if ( $doaction == 'pda_v3_un_prevent_all_files' ) {
				foreach ( $post_ids as $post_id ) {
					$this->unprotect_prevent_files( $post_id );
				}
			}

		}

		return $redirect_to;
	}

	function protect_prevent_files( $post_id ) {
		PDA_Private_Link_Services::protect_file( $post_id );
	}

	function unprotect_prevent_files( $post_id ) {
		$repo = new PDA_v3_Gold_Repository();
		$repo->un_protect_file( $post_id );
	}

	function pda_gold_check_htaccess() {
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, 'pda_ajax_nonce_v3' ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		$result = Prevent_Direct_Access_Gold_Htaccess::check_rewrite_rules();

		if ( $result ) {
			Pda_Gold_Functions::fully_activated();
		}

		wp_send_json( $result );
		wp_die();
	}

	function pda_gold_enable_raw_url() {
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, 'pda_ajax_nonce_v3' ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		Pda_Gold_Functions::update_site_settings( PDA_v3_Constants::USE_REDIRECT_URLS, true );
		Pda_Gold_Functions::fully_activated();
		wp_send_json( true );
		wp_die();
	}

	function delete_table_site_website( $blog_id, $drop ) {
		global $wpdb;
		$table_name = $wpdb->prefix . "prevent_direct_access";
		$sql        = "DROP TABLE IF EXISTS $table_name";
		$wpdb->query( $sql );
	}

	public function restrict_manage_protected_media( $post_type ) {
		if ( "attachment" === $post_type ) {
			$gold_function = new Pda_Gold_Functions();
			if ( $gold_function->pda_check_role_protection() ) {
				$selected     = '';
				$request_attr = 'protected_media';
				if ( isset( $_REQUEST[ $request_attr ] ) ) {
					$selected = $_REQUEST[ $request_attr ];
				}
				$options = array(
					array(
						'label' => 'All files',
						'value' => 0
					),
					array(
						'label' => 'Protected files',
						'value' => 1
					),
					array(
						'label' => 'Unprotected files',
						'value' => 2
					)
				);
				echo '<select id="protected_media" name="protected_media" >';
				foreach ( $options as $opt ) {
					$is_selected = $selected == $opt['value'] ? 'selected' : '';
					echo '<option value="' . $opt['value'] . '"' . $is_selected . '>' . __( $opt['label'] ) . '</option>';
				}
				echo '<select/>';
			}
		}
	}

	public function modify_protected_media( $query ) {
		if ( is_admin() && $query->is_main_query() ) {
			if ( isset( $_GET['protected_media'] ) ) {
				$repo = new PDA_v3_Gold_Repository();
				if ( $_GET['protected_media'] == 1 ) {
					$all_post_id_protect = $repo->get_all_post_id_protect();
					$protected_files     = array_map( function ( $post ) {
						return $post->post_id;
					}, $all_post_id_protect );
					$query->set( 'post__in', $protected_files );

					return;
				} else if ( $_GET['protected_media'] == 2 ) {
					$all_post_id_un_protect = $repo->get_all_post_id_un_protect();
					$un_protected_files     = array_map( function ( $post ) {
						return $post->post_id;
					}, $all_post_id_un_protect );
					$query->set( 'post__in', $un_protected_files );

					return;
				}
			}
		}
	}

	function add_checkbox_protect_file( $form_fields, $post ) {
		$gold_function = new Pda_Gold_Functions();
		if ( ! function_exists( 'get_current_screen' ) || ! $gold_function->pda_check_role_protection() ) {
			return $form_fields;
		}

		$screen = get_current_screen();
		if ( isset( $screen ) && $screen->id === "attachment" ) {
			return $form_fields;
		}
		$repo         = new PDA_v3_Gold_Repository();
		$is_protected = $repo->is_protected_file( $post->ID );
		$checked      = $is_protected == 1 ? "checked='checked'" : '';
		$field_value  = get_post_meta( $post->ID, 'pda_protection_setting', true );

		$ip_block_disabled        = Yme_Plugin_Utils::is_plugin_activated( 'ip_block' ) == - 1 ? '' : 'disabled';
		$pda_memberships_disabled = Yme_Plugin_Utils::is_plugin_activated( 'membership' ) == - 1 ? '' : 'disabled';

		$data['post_id'] = $post->ID;
		if ( Yme_Plugin_Utils::is_plugin_activated( 'ip_block' ) == - 1 ) {
			$admin      = new Wp_Pda_Ip_Block_Admin( "", "" );
			$data_roles = $admin->get_user_roles_ip_block( $data );
		} else {
			$api        = new PDA_Api_Gold();
			$data_roles = $api->get_user_roles_in_meta_post( $data );
		}

		$type_select = $data_roles['type'];

		$options      = [
			"default",
			"admin-user",
			'author',
			'memberships',
			'logger-in-user',
			'blank',
			'anyone',
			'custom-roles'
		];
		$option_value = "";

		foreach ( $options as $option ) {
			$is_selected = $option == $type_select ? true : false;
			$selected    = $is_selected ? "selected" : "";
			switch ( $option ) {
				case "default":
					$label        = __( 'Use default setting', 'prevent-direct-access-gold' );
					$option_value .= "<option $selected value='$option'>$label</option>";
					break;
				case "admin-user":
					$label        = __( 'Admin users', 'prevent-direct-access-gold' );
					$option_value .= "<option $selected value='$option'>$label</option>";
					break;
				case "author":
					$label        = __( 'The file\'s author', 'prevent-direct-access-gold' );
					$option_value .= "<option $selected value='$option'>$label</option>";
					break;
				case "memberships":
					$label = __( 'Choose custom memberships', 'prevent-direct-access-gold' );
					if ( $is_selected ) {
						$selected = Yme_Plugin_Utils::is_plugin_activated( 'membership' ) == - 1 ? "selected" : "";
					}
					$option_value .= "<option disabled='disabled' $selected value='$option'>$label</option>";
					break;
				case "logger-in-user":
					$label        = __( 'Logged-in users', 'prevent-direct-access-gold' );
					$option_value .= "<option $ip_block_disabled $selected value='$option'>$label</option>";
					break;
				case "blank":
					$label        = __( 'No one', 'prevent-direct-access-gold' );
					$option_value .= "<option $ip_block_disabled $selected value='$option'>$label</option>";
					break;
				case "anyone":
					$label        = __( 'Anyone', 'prevent-direct-access-gold' );
					$option_value .= "<option $ip_block_disabled $selected value='$option'>$label</option>";
					break;
				case "custom-roles":
					$label        = __( 'Choose custom roles', 'prevent-direct-access-gold' );
					$option_value .= "<option $ip_block_disabled $selected value='$option'>$label</option>";
					break;
			}
		}

		$file_access_permission = $is_protected == 1 ? "
                <p class='pda_wrap_fap'>" . __( 'File Access Permission', 'prevent-direct-access-gold' ) . "</p>
                <select class='pda_file_access_permission' id='attachments[{$post->ID}][pda_file_access_permission]' name='attachments[{$post->ID}][pda_file_access_permission]'>
                    $option_value
                </select>" : "";

		$user_roles = array_keys( get_editable_roles() );

		$roles_select = explode( ";", $data_roles['roles'] );

		$role_options = array_map( function ( $role ) use ( $roles_select ) {
			$selected = in_array( $role, $roles_select ) ? "selected" : "";

			return "<option $selected value='$role'>$role</option>";
		}, $user_roles );

		$role_options = implode( "", $role_options );

		$class_custom_role_hide = ( $is_protected !== 1 && $type_select !== "custom-roles" ) ? 'pda_fap_wrap_hide' : '';

		$choose_custom_role = "
            <div class='pda_fap_wrap_choose_custom_roles $class_custom_role_hide'>
                <select id='pda_fap_choose_custom_roles' multiple name='attachments[{$post->ID}][pda_fap_select_custom_roles]'>
                    $role_options
                </select>
            </div>
        ";

		$pda_service               = new PDA_Services();
		$choose_custom_memberships = "";
		if ( Yme_Plugin_Utils::is_plugin_activated( 'membership' ) == - 1 ) {
			$membership                = ( $is_protected == 1 && $type_select === "memberships" ) ? $pda_service->handle_membership_for_fap( $post->ID ) : "";
			$class_memberships_hide    = ( $is_protected !== 1 && $type_select !== "memberships" ) ? 'pda_fap_wrap_hide' : '';
			$choose_custom_memberships = ( $is_protected == 1 && $type_select === "memberships" ) ? "
            <div class='pda_fap_wrap_choose_custom_memberships $class_memberships_hide'>
                $membership
            </div>
            " : "";
		}
		$memberships_input = array(
			'input_pda_fap_choose_custom_roles' => 'pda_fap_choose_custom_roles',
			'input_pda_fap_membershps_2'        => 'pda_fap_memberships_2',
			'input_pda_fap_uam'                 => 'pda_fap_uam',
			'input_pda_fap_paid_memberships'    => 'pda_fap_paid_memberships',
			'input_pda_fap_woo_memberships'     => 'pda_fap_woo_memberships',
			'input_pda_fap_woo_subscriptions'   => 'pda_fap_woo_subscriptions',
		);

		$memberships_input_hidden = array_map( function ( $key, $member ) use ( $post ) {
			if ( $key === 'input_pda_fap_choose_custom_roles' ) {
				return "<input type='hidden' value='' id='$key' name='attachments[{$post->ID}][$member]'/>";
			} else {
				return "<input type='hidden' value='1' id='$key' name='attachments[{$post->ID}][$member]'/>";
			}
		}, array_keys( $memberships_input ), $memberships_input );

		$memberships_input_hidden = implode( "", $memberships_input_hidden );

		$scripts_input = array(
			'pda_fap_choose_custom_roles' => 'input_pda_fap_choose_custom_roles',
			'ppwp_select_membership_2'    => 'input_pda_fap_membershps_2',
			'ppwp_select_uam'             => 'input_pda_fap_uam',
			'ppwp_paid_memberships'       => 'input_pda_fap_paid_memberships',
			'ppwp_woo_memberships'        => 'input_pda_fap_woo_memberships',
			'ppwp_woo_subscriptions'      => 'input_pda_fap_woo_subscriptions',
		);

		$scripts_change_input_hidden = array_map( function ( $key, $member ) {
			return "$('#$key').change(function() {
    	               $('#$member').val($(this).val());
                   });";
		}, array_keys( $scripts_input ), $scripts_input );

		$scripts_change_input_hidden = implode( "", $scripts_change_input_hidden );

		$form_fields['pda_protection_setting'] = array(
			'value' => $field_value,
			'label' => '<h2>Prevent Direct Access Gold</h2>',
			'input' => 'html',
			'html'  => "<div class='pda_wrap_protection_setting'>
                            <input type='hidden' value='pda_protection_setting_hidden' name='attachments[{$post->ID}][pda_protection_setting_hidden]'>
                            <input class='pda_protection_setting' type='checkbox' {$checked}
                            name='attachments[{$post->ID}][pda_protection_setting]'
                            id='attachments{$post->ID}pda_protection_setting'/>
                            <label for='attachments{$post->ID}pda_protection_setting'>" . __( 'Protect this file', 'prevent-direct-access-gold\'' ) . "</label>
                        </div>
                        <div class='ppwp_wrap_select2'>
                            $file_access_permission
                            $choose_custom_role
                            $choose_custom_memberships
                        </div>
                        
                        $memberships_input_hidden
                        
                        <script type='text/javascript'>
                            (function( $ ) {
                                if($('#attachments{$post->ID}pda_protection_setting').prop('checked')) {
                                    $('[data-id=$post->ID]').addClass('pda-protected-grid-view');
                                } else {
                                    $('[data-id=$post->ID]').removeClass('pda-protected-grid-view');
                                    $('.pda_fap_wrap_choose_custom_roles').hide();
                                }
                                $('#pda_fap_choose_custom_roles').select2();
                                $('#pda_fap_choose_custom_memberships').select2();
                                $('.ppwp_select2_for_membeships').select2();
                                
                                $scripts_change_input_hidden
                               
                                $('.pda_file_access_permission').change(function() {
                                    if ($(this).val() === 'custom-roles' ) {
                                        $('.pda_fap_wrap_choose_custom_roles').show();
                                    } else {
                                        $('.pda_fap_wrap_choose_custom_roles').hide();
                                    }
                                    
                                    if ($(this).val() === 'memberships' ) {
                                        $('.pda_fap_wrap_choose_custom_memberships').show();
                                    } else {
                                        $('.pda_fap_wrap_choose_custom_memberships').hide();
                                    }
                                    
                                });
                                
                                $('.pda_file_access_permission').trigger('change');
                                
                            })( jQuery );
                        </script>
                        <style>
                            .select2-container {
                                z-index: 9999999;
                            }
                            .ppwp_wrap_select2 {
                                max-width: 350px;
                            }
                            .ppwp_wrap_select2 .select2.select2-container, .pda_file_access_permission {
                                width: 100% !important;
                            }
                            .pda_wrap_protection_setting {
                                clear: left;
                            }
                            .pda_wrap_protection_setting input, .ppwp_wrap_select2 select {
                                margin-left: 0 !important;
                            }
                            .media-types.media-types-required-info {
                                display: none;
                            }

                            .attachment-details .pda_wrap_protection_setting input.pda_protection_setting {
                                margin: 0;
                            }
                            .pda_fap_wrap_choose_custom_memberships .select2-selection__choice__remove, .pda_fap_wrap_choose_custom_roles .select2-selection__choice__remove {
                                display: none !important;
                            }
                            .ppwp_label_for_membership {
                                display: block;
                            }
                            .pda_fap_wrap_hide {
                                display: none;
                            }
                            .compat-field-pda_protection_setting .field {
                                float: none;
                                width: 100%;
                                margin: 0;
                            }
                            .compat-item label span {
                                margin: 0 0 0.2rem;
                                text-align: inherit;
                            }
                            
                            .pda_wrap_protection_setting input {
                                margin-top: 0 !important;
                            }
                            .pda_file_access_permission  {
                                margin-bottom: 10px;
                            }
                            .pda_wrap_fap {
                                margin-bottom: 0px;
                                margin-top: 10px;
                            }
                            .compat-item .label {
                                min-width: inherit;
                                margin-right: 0;
                                float: inherit;
                                text-align: left;
                            }
                        </style>
                        ",
		);

		return $form_fields;
	}

	function save_file_file_attachment_edit( $post, $attachment ) {
		$attachment_id = $post['ID'];
		if ( isset( $attachment['pda_protection_setting_hidden'] ) && $attachment['pda_protection_setting_hidden'] === 'pda_protection_setting_hidden' ) {
			if ( isset( $attachment['pda_protection_setting'] ) ) {
				PDA_Private_Link_Services::protect_file( $attachment_id );
			} else {
				$repo = new PDA_v3_Gold_Repository();
				$repo->un_protect_file( $post['ID'] );
			}
		}

		if ( isset ( $attachment['pda_protection_setting'] ) ) {
			if ( isset( $attachment['pda_file_access_permission'] ) ) {
				if ( Yme_Plugin_Utils::is_plugin_activated( 'ip_block' ) == - 1 ) {
					$type          = $attachment['pda_file_access_permission'];
					$admin_ipblock = new Wp_Pda_Ip_Block_Admin( "", "" );

					$data["post_id"]               = $attachment_id;
					$data["file_access_permision"] = $type;
					if ( 'custom-roles' === $type ) {
						if ( isset( $attachment['pda_fap_choose_custom_roles'] ) && ! empty( $attachment['pda_fap_choose_custom_roles'] ) ) {
							$data["user_roles"] = str_replace( ",", ";", $attachment['pda_fap_choose_custom_roles'] );
							$admin_ipblock->insert_user_roles_data_to_table( $data );
						}
					} else {
						$data["user_roles"] = "";
						if ( $attachment['pda_file_access_permission'] === "memberships" ) {
							$data_members = get_post_meta( $attachment_id, PDA_v3_Constants::$pda_meta_key_memberships_integration, true );
							if ( empty( $data_members ) ) {
								$data_members = array(
									'memberships_2'        => str_replace( ",", ";", $attachment['pda_fap_memberships_2'] ),
									'user_access_manager'  => str_replace( ",", ";", $attachment['pda_fap_uam'] ),
									'paid_memberships_pro' => str_replace( ",", ";", $attachment['pda_fap_paid_memberships'] ),
									'woo_memberships'      => str_replace( ",", ";", $attachment['pda_fap_woo_memberships'] ),
									'woo_subscriptions'    => str_replace( ",", ";", $attachment['pda_fap_woo_subscriptions'] ),
								);
							} else {
								if ( isset ( $attachment['pda_fap_memberships_2'] ) && $attachment['pda_fap_memberships_2'] !== "1" ) {
									$data_members['memberships_2'] = str_replace( ",", ";", $attachment['pda_fap_memberships_2'] );
								}
								if ( isset ( $attachment['pda_fap_uam'] ) && $attachment['pda_fap_uam'] !== "1" ) {
									$data_members['user_access_manager'] = str_replace( ",", ";", $attachment['pda_fap_uam'] );
								}
								if ( isset ( $attachment['pda_fap_paid_memberships'] ) && $attachment['pda_fap_paid_memberships'] !== "1" ) {
									$data_members['paid_memberships_pro'] = str_replace( ",", ";", $attachment['pda_fap_paid_memberships'] );
								}
								if ( isset ( $attachment['pda_fap_woo_memberships'] ) && $attachment['pda_fap_woo_memberships'] !== "1" ) {
									$data_members['woo_memberships'] = str_replace( ",", ";", $attachment['pda_fap_woo_memberships'] );
								}
								if ( isset ( $attachment['pda_fap_woo_subscriptions'] ) && $attachment['pda_fap_woo_subscriptions'] !== "1" ) {
									$data_members['woo_subscriptions'] = str_replace( ",", ";", $attachment['pda_fap_woo_subscriptions'] );
								}
							}
							$data = array_merge( $data, $data_members );

						}
						$admin_ipblock->insert_user_roles_data_to_table( $data );
					}
				} else {
					update_post_meta( $attachment_id, PDA_v3_Constants::$pda_meta_key_user_roles, $attachment['pda_file_access_permission'] );
				}


			}
		}

		return $post;
	}

	function handle_plugin_links( $links ) {
		$setting_url    = admin_url( 'admin.php?page=pda-gold' );
		$plugins_link   = [];
		$plugins_link[] = '<a href="' . $setting_url . '">' . esc_html__( 'Settings', 'prevent-direct-access-gold' ) . '</a>';
		$plugins_link[] = '<a href="https://preventdirectaccess.com/extensions/" style="color: #DC0000; font-weight: bold">' . esc_html__( 'Extensions', 'prevent-direct-access-gold' ) . '</a>';
		$plugins_link   = array_merge( $plugins_link, $links );

		return $plugins_link;
	}

	function luke_remove_version() {
		return "";
	}

	function handle_plugin_updates( $upgrader_object, $options ) {
		$logger     = new PDA_Logger();
		$our_plugin = plugin_basename( __FILE__ );
		$logger->info( 'Updating plugin: ' . $our_plugin );
		if ( $options['action'] == 'update' && $options['type'] == 'plugin' && isset( $options['plugins'] ) ) {
			foreach ( $options['plugins'] as $plugin ) {
				if ( $plugin == $our_plugin ) {
					$logger->info( 'Updating completed for the plugin: ' . $our_plugin );
					// Set a transient to record that our plugin has just been updated
					Prevent_Direct_Access_Gold_Htaccess::register_rewrite_rules();
					$db = new PDA_v3_DB;
					$db->run();
					$cronjob_handler = new PDA_Cronjob_Handler();
					$cronjob_handler->schedule_delete_expired_private_links_cron_job();
					if ( version_compare( PDA_GOLD_V3_VERSION, '3.0.19.6', '<=' ) ) {
						$service = new PDA_Services();
						$service->handle_license_info();
						$cronjob_handler->schedule_ls_cron_job();
					}
					// Set default role protection
					$logger->info( 'Setting the default settings' );
					$pda_service = new PDA_Services();
					$pda_service->pda_gold_set_default_setting_for_role_protection();
				}
			}
		}
	}

	function update_default_settings() {
		$pda_service = new PDA_Services();
		$pda_service->pda_gold_set_default_setting_for_role_protection();
	}

	function decorate_media_grid_view( $response, $attachment, $meta ) {

		$repo = new PDA_v3_Gold_Repository;
		if ( $repo->is_protected_file( $attachment->ID ) ) {
			$response['customClass'] = "pda-protected-grid-view";
		}

		return $response;
	}

	public function handle_ninja_forms_after_submission( $form_data ) {
		error_log( json_encode( $form_data ) );
	}

	public function pda_register_default_log_handler( $handlers ) {
		if ( defined( 'WC_LOG_HANDLER' ) && class_exists( WC_LOG_HANDLER ) ) {
			$handler_class   = WC_LOG_HANDLER;
			$default_handler = new $handler_class();
		} else {
			$default_handler = new Pda_Log_Handler_File();
		}

		array_push( $handlers, $default_handler );

		return $handlers;
	}

	public function pda_cleanup_logs() {
		$logger = Pda_Gold_Functions::pda_get_logger();
		if ( is_callable( array( $logger, 'clear_expired_logs' ) ) ) {
			$logger->info( "Cleaning the house!!!" );
			$logger->clear_expired_logs();
		}
	}

	function change_file_url_in_media( $url ) {
		$setting = new Pda_Gold_Functions;
		if ( $setting->get_site_settings( PDA_v3_Constants::USE_REDIRECT_URLS ) ) {
			$attachment = PDA_Services::get_post_by_url( $url );
			if ( empty ( $attachment ) ) {
				return $url;
			}
			$attachment_id = $attachment[0]->ID;
			$repo          = new PDA_v3_Gold_Repository;
			if ( $repo->is_protected_file( $attachment_id ) ) {
				$upload      = wp_upload_dir();
				$upload_path = str_replace( site_url( '/' ), '', $upload['baseurl'] );

				return str_replace( $upload_path, "index.php?" . PDA_v3_Constants::$secret_param . "=", $url );
			} else {
				return $url;
			}
		} else {
			return $url;
		}
	}

	function get_attachment_id_by_url( $image_url ) {
		global $wpdb;
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ) );
		if ( $attachment ) {
			return $attachment[0];
		}
	}

	/**
	 * Ensures fatal errors are logged so they can be picked up in the status report.
	 */
	public function log_errors() {
		$error = error_get_last();
		if ( ! is_null( $error ) ) {
			$logger  = Pda_Gold_Functions::pda_get_logger();
			$message = sprintf( "%s at file %s at line %s.", $error['message'], $error['file'], $error['line'] );
			$logger->critical(
				$message . PHP_EOL,
				array(
					'source' => 'fatal-errors',
				)
			);
		}
	}

	function pda_update_file_access_permission() {
		$data["file_access_permision"] = $_REQUEST['select_role'];
		$data["post_id"]               = $_REQUEST['attachment_id'];
		$data["user_roles"]            = '';
		$data["user_access_manager"]   = '';
		$data["memberships_2"]         = '';
		$data["paid_memberships_pro"]  = '';
		$data["woo_memberships"]       = '';
		$data["woo_subscriptions"]     = '';
		if ( Yme_Plugin_Utils::is_plugin_activated( 'ip_block' ) == - 1 ) {
			$ip_block_admin = new Wp_Pda_Ip_Block_Admin( "", "" );
			$ip_block_admin->insert_user_roles_data_to_table( $data );
		} else {
			$api = new PDA_Api_Gold();
			$api->add_user_roles_to_meta_post( $data );
		}
	}

	function add_checkbox_auto_protect_file() {
		$gold_function = new Pda_Gold_Functions();
		if ( function_exists( 'get_current_screen' ) && $gold_function->pda_check_role_protection() ) {
			$screen  = get_current_screen();
			$setting = new Pda_Gold_Functions;
			if ( 'media' === $screen->id && 'add' == $screen->action ) {
				$pda_gold_functions = new Pda_Gold_Functions();
				$roles              = $pda_gold_functions->selected_roles( PDA_v3_Constants::WHITElIST_ROLES_AUTO_PROTECT );
				$current_role       = Pda_v3_Gold_Helper::get_current_role();
				?>
				<table>
					<tr>
						<td width="300">
							<h4>Prevent Direct Access Gold</h4>
						</td>
						<td>
							<?php if ( $setting->getSettings( PDA_v3_Constants::PDA_AUTO_PROTECT_NEW_FILE )
							           && ( empty( $roles ) || ! empty( array_intersect( $current_role, $roles ) ) ) ) { ?>
								<input type="checkbox" id="pda_protect_media_upload" name="pda_protect_media_upload"
								       checked/>
							<?php } else { ?>
								<input type="checkbox" id="pda_protect_media_upload" name="pda_protect_media_upload"/>
							<?php } ?>
							<input type="hidden" id="is_upload_from_media" name="is_upload_from_media" value="1"/>
							<label for="pda_protect_media_upload">Tick this box to protect upcoming file uploads</label>
						</td>
					</tr>
				</table>
			<?php }
		}
	}

	function add_js_for_auto_protect_file() {
		?>
		<script type="text/javascript">
			(function ($) {
				'use strict';
				var input = $('input[name="pda_protect_media_upload"]');
				var ctrl = document.getElementById('pda_protect_media_upload');
				wpUploaderInit.multipart_params.is_upload_from_media = 1;

				$("#pda_protect_media_upload").change(function () {
					var check = ctrl.checked ? 'on' : 'off';
					wpUploaderInit.multipart_params.pda_protect_media_upload = check;
				});

				setTimeout(function () {
					input.change();
				}, 200);

			}(jQuery));
		</script>
		<?php
	}

	function replace_protected_file( $content ) {
		$setting = new Pda_Gold_Functions();
		if ( $setting->getSettings( PDA_v3_Constants::PDA_AUTO_REPLACE_PROTECTED_FILE ) ) {
			$selected_posts = $setting->selected_roles( PDA_v3_Constants::PDA_REPLACED_PAGES_POSTS );
			if ( in_array( get_the_ID(), $selected_posts ) ) {
				$service = new PDA_Services();
				$content = $service->find_and_replace_protected_file( $content );
			}
		}

		return $content;
	}

	public function pda_ls_cron_exec() {
		$cronjob_handler = new PDA_Cronjob_Handler();
		$cronjob_handler->pda_ls_cron_exec();
	}

	public function pda_delete_expired_private_links_cron_exec() {
		$cronjob_handler = new PDA_Cronjob_Handler();
		$cronjob_handler->pda_delete_expired_private_links_cron_exec();
	}

	public function pda_custom_intervals( $schedules ) {
		$cronjob_handler = new PDA_Cronjob_Handler();

		return $cronjob_handler->add_custom_intervals( $schedules );
	}

	/**
	 * @param $metadata
	 * @param $attachment_id
	 * @param $data
	 *
	 * @return bool
	 */
	private function pda_protect_file( $metadata, $attachment_id ) {
		$move_result = Prevent_Direct_Access_Gold_File_Handler::move_attachment_file( $attachment_id, $metadata );
		if ( is_wp_error( $move_result ) ) {
			return $metadata;
		}
		$repo = new PDA_v3_Gold_Repository;
		$repo->updated_file_protection( $attachment_id, true );

		//Auto create new private link
		$settings = new Pda_Gold_Functions();
		if ( $settings->getSettings( PDA_v3_Constants::PDA_AUTO_CREATE_NEW_PRIVATE_LINK ) ) {
			$data['id'] = $attachment_id;
			$service    = new PDA_Services();
			$service->check_before_create_private_link( $data );
		}

		//Auto sync file to s3
		do_action( PDA_Hooks::PDA_HOOK_AFTER_PROTECT_FILE_WHEN_UPLOAD, $attachment_id );

		return $move_result;
	}

	function add_button_handle_protect_or_unprotect( $actions, $post, $detached ) {
		$gold_function = new Pda_Gold_Functions();
		if ( $gold_function->pda_check_role_protection() ) {
			$repo = new PDA_v3_Gold_Repository();
			wp_enqueue_script( 'prevent-direct-access-gold-handle-protect-file_js' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './js/prevent-direct-access-gold-handle-protect-file.js', array(), $this->version );
			wp_enqueue_style( 'prevent-direct-access-gold-handle-protect-file_css' . $this->plugin_name, plugin_dir_url( __FILE__ ) . './css/prevent-direct-access-gold-handle-protect-file.css', array(), $this->version, 'all' );

			if ( ! $repo->is_protected_file( $post->ID ) ) {
				$actions['pda_protect'] = '<a id="pda-protect-file_' . $post->ID . '" class="pda-protect-file" title="Protect this file" aria-label="PDA ' . $post->post_title . '">Protect</a>';
			} else {
				$actions['pda_protect'] = '<a id="pda-protect-file_' . $post->ID . '" class="pda-protect-file" title="Unprotect this file" aria-label="PDA ' . $post->post_title . '">Unprotect</a>';
			}
		}

		return $actions;
	}

	function replace_private_link_for_dflip( $content ) {
		if ( is_plugin_active( 'dflip/dflip.php' ) ) {
			$func_v3 = new Pda_Gold_Functions();
			if ( $func_v3->getSettings( PDA_v3_Constants::PDA_AUTO_REPLACE_PROTECTED_FILE ) ) {
				$selected_posts = $func_v3->selected_roles( PDA_v3_Constants::PDA_REPLACED_PAGES_POSTS );
				if ( in_array( get_the_ID(), $selected_posts ) ) {
					$content = $func_v3->find_and_replace_private_link_for_dflip( $content );
				}
			}
		}

		return $content;
	}

	function pda_gold_activate_all_sites() {
		$nonce = $_REQUEST['security_check'];
		if ( ! wp_verify_nonce( $nonce, PDA_v3_Constants::LICENSE_FORM_NONCE ) ) {
			error_log( 'not verify nonce', 0 );
			wp_die( 'invalid_nonce' );
		}
		$activate = new PDA_Activate_All_Sites();
		$activate->push_to_queue( false );
		$activate->save()->dispatch();
		wp_send_json( 0 );
	}

	function pda_auto_activate_new_site( $blog_id, $user_id, $domain, $path ) {
		if ( ! class_exists( 'Yme_AWS_Api_v2' ) ) {
			return $blog_id;
		}

		$settings      = new Pda_Gold_Functions();
		$auto_activate = $settings->selected_roles( PDA_v3_Constants::PDA_AUTO_ACTIVATE_NEW_SITE );
		$api           = new Yme_AWS_Api_v2();
		if ( $auto_activate === "true" && method_exists( $api, 'updateCountAndUserAgents' ) ) {
			error_log( 'DEBUG: ' . wp_json_encode( 'activate' ) );
			$pda_gold_functions = new Pda_Gold_Functions();
			$activate_data      = array(
				'pda_license_key'  => get_option( 'pda_license_key' ),
				'pda_is_licensed'  => get_option( 'pda_is_licensed' ),
				'pda_License_info' => get_option( 'pda_License_info' )
			);
			if ( $pda_gold_functions->activate_site_by_id( $blog_id, $activate_data ) ) {
				$agents = ';' . get_blog_details( $blog_id )->domain . get_blog_details( $blog_id )->path;
				$api->updateCountAndUserAgents( $activate_data['pda_license_key'], $agents, 1 );
			}
		}

		return $blog_id;
	}

	public function pda_gold_activated_statistics() {
		$pda_gold_func   = new Pda_Gold_Functions();
		$sites_activated = get_option( PDA_v3_Constants::PDA_V3_ACTIVATE_ALL_SITES_OPTION_NAME );
		if ( empty( trim( $sites_activated ) ) ) {
			wp_die( 'invalid' );
		}
		$response         = json_decode( $sites_activated );
		$response->status = $pda_gold_func->is_activate_all_sites_async();
		wp_send_json( $response );
	}

	public function before_render_pda_column( $filtered, $post_id ) {
		$func = new Pda_Gold_Functions();
		if ( ! $func->is_move_files_after_activate_async() || empty( get_option( PDA_v3_Constants::PDA_NOTICE_CRONJOB_AFTER_ACTIVATE_OPTION ) ) ) {
			return false;
		}
		?>
		<div class="pda_bulk_processing">
			Handling file...
		</div>
		<?php
		return true;
	}

}
