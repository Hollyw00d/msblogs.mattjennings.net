# prevent-direct-access-gold-3.0

Build status: [![CircleCI](https://circleci.com/bb/ymese_dev/prevent-direct-access-gold-3.0/tree/master.svg?style=svg&circle-token=cdb8e7e16511db7f9fda02a94c456fca364935e6)](https://circleci.com/bb/ymese_dev/prevent-direct-access-gold-3.0/tree/master)

**Getting started**

- Front-end 

```bash
git submodule update --init
git submodule update --remote
cd client/pda-ui
npm install
npm run plan
npm run start
```

Enjoy!

**IIS Rewrite Rules**

```
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
  <system.webServer>
    <rewrite>
      <rules>
                <rule name="pda-original-link" patternSyntax="ECMAScript">
                    <match url="wp-content/uploads(/_pda/.*\.\w+)" />
                    <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="/index.php?pda_v3_pf={R:1}" />
                </rule>
                <rule name="pda-private-link" patternSyntax="ECMAScript">
                    <match url="private/([a-zA-Z0-9-_]+)" />
                    <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="/index.php?pda_v3_pf={R:1}&amp;pdav3_rexypo=ymerexy" />
                </rule>
                <rule name="wordpress" patternSyntax="Wildcard">
                    <match url="*" />
                    <conditions logicalGrouping="MatchAll" trackAllCaptures="false">
                        <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="index.php" />
                </rule>
      </rules>
    </rewrite>
  </system.webServer>
</configuration>
```

**Changelogs**

**version 3.0.24.8: 

- [Improvement] Use wp_option to check data migration

**version 3.0.24.7: February 25, 2019**

- [Hotfix] Logic checking for data migration from version 2.x.x

**version 3.0.24.6: January 22, 2019**

- [Improvement] Using custom function to get mime type

**version 3.0.24.5: January 16, 2019**

- [Feature] Added a public service in PDA Gold to fetch the protected file
- [Feature] PDA Memberships: Separate custom membership from the default FAP dropdown
- [Feature] PDA Gold - IP Block: protect all files on a selected folder

**version 3.0.24.4: December 26, 2018**

- [BugFix] Add debug log for $_SERVER object

**version 3.0.24.3: December 26, 2018**

- [BugFix] Add debug log when streaming the video

**version 3.0.24.2: December 21, 2018**

- [BugFix] Super Administrator cannot apply control protection

**version 3.0.24.1: December 21, 2018**

[BugFix] Fix cannot set default user control protect's value  

**version 3.0.24: December 19th, 2018**

- [Feature] Settings option to specify who can change the file protection
- [Improvement] When the media file is HTML type, the protected or private links should show in the browser. Now it automatically downloads by browser. 
- [Improvement] allow "Choose custom roles & memberships" FAP on "Add Media"
- [Improvement] Change the Protection file component when the file is backing up
- [BugFix] Auto-update unprotected URLs in content - not full file URLs
- [BugFix] The total number of protected and unprotected files isn't equal the number of all files
- [Improvement] Enhance 404 redirection
- [BugFix] iFAP displays as Custom Memberships even though No role is chosen

**version 3.0.23.1: December, 14th, 2018**

- [BugFix] Unprotect files by bulk action does not update the post meta 

**version 3.0.23: December, 7th, 2018**

- [Feature] Auto-activate license on sub-sites
- [Feature] Protect file uploads by user roles does not apply for Super Admin at normal sites
- [Improvement] Choose "Admin users" is default setting of FAP in multisite mode

**version 3.0.22: November, 30th, 2018**

- [Improvement] Better license handling
- [Feature] Add "Protect" button on file actions
- [Feature] Change download links to S3 Signed URLs once synced/offloaded
- [Improvement] Choose "Admin users" is default setting of FAP
- [Improvement] Show warning notification when the PHP's version is lower than 5.5
- [BugFix] PDA Gold: FAP seem doesn't work in multisite mode
- [Improvement] PDA logo in WP menu

**version 3.0.21.5: November 22th, 2018**

- [BugFix] Cannot protect/un-protect from media grid view

**version 3.0.21.4: November 22th, 2018**

- [BugFix] Do not cache for streaming video

**version 3.0.21.3: November 21th, 2018**

- [Improvement] Clear ob flush cache

**version 3.0.21.2: November 16th, 2018**

- [HotFix] Update the background process's name to remove duplication 

**version 3.0.21.1: November 8th, 2018**

- [BugFix] Replace nonce attribute by wp_nonce_field function to fix the bug cannot get the nonce value in chrome (using nonce attribute)

**version 3.0.21: November 6th, 2018**

- [Feature] Auto protect files upload by certain user roles

**version 3.0.20.1: October 29, 2018**

- [BugFix] Cannot get the backup protection when re-activate the plugin

**version 3.0.20: October 26, 2018**

- [Improvement] Add force download option
- [Improvement] Revamp setting UI and file status on media file
- [BugFix] Cannot show some pages or posts

**version 3.0.19.11: October 19, 2018**

- [Improvement] Protected files should have the red shadow borders with other Custom Post Types
- [Improvement] Add hook before running the file status.
- [Improvement] Handle errors  - exception and show the error message to client with PDA S3 Integration

**version 3.0.19.10: October 17, 2018**

- [Hotfix] Integrate with old pda-s3 plugin

**version 3.0.19.9: October 16, 2018**

- [Improvement] Add hook before and after protect file
- [Improvement] When un-protect file, DO NOT unsync file
- [BugFix] Protected files don't have the red shadow borders 
- [BugFix] Private links don't work when Protect Videos extension is enabled
- [Improvement] Showing purchased add-ons on license tab
- [Improvement]Hide File Access Permission (status button and tab) if the file is deleted from Server

**version 3.0.19.8: October 15, 2018**

- [Improvement] Generate pot file

**version 3.0.19.7: October 9, 2018**

- Fix mp3 file private link cannot play

**version 3.0.19.6: October 8, 2018**

- Fix conflict with BB Pagebuilder

- Shared Private Link is still active when the plugin was inactive

- There are more than one default private link

**version 3.0.19.5.1: October 3, 2018**

- Fix bug, protected file returning blank page (https://app.asana.com/0/770983580839219/848525529272467)

**version 3.0.19.5: September 28, 2018**

- When get baseurl using wp_upload_dir need a trick to replace http by https to support the older WP version

- Provide API to integrate with other plugins

- Provide a hook when clicking on private links

**version 3.0.19.4.2: September 28, 2018**

- Hotfix for private links expired after changing expiry date

**version 3.0.19.4.1: September 26, 2018**

- Hotfix for file access permission logic

**version 3.0.19.4: September 25, 2018**

- Replace exiting embedded images/files on the content

- Fix conflicts with WP Media Folder

**version 3.0.19.3.1: September 25, 2018**

- Hotfix for hotlinking rules to get the home url

**version 3.0.19.3**

- Fix the performance issues belonged to filters function

**version 3.0.19.2**

- Improve UI for Shared Private Link

- Fix bug in "Shared Private Link": Download expiry time is different from Created times after editing 

- Inconsistent download expiry's timestamp when editing vs creating custom private links

- Control which files to be auto-protected on "Media" upload - on the fly

**version 3.0.19.1**

- Fix css for metabox 

**version 3.0.19**

- Generate a unique private link for users

- [PDAv3]If user choose the raw options then using the ~/?rest_route=/. instead of /wp-json for WordPress REST API route.

-[PDAv3]Update "none" value in X-Robots-Tag's header

- Re-arrange the settings option for "Enable Debug Log"

- Gold 3.0: integrate File Access Permission into file popup & attachment page 

**version 3.0.18.7**

- Add debug log when handle the private link
 
**version 3.0.18.6**

- Add log log when sending file to client

**version 3.0.18.5**

- Fix filter for protected and unprotected files

- Improve css for "Is it protected?" checkbox

- Update yme-plugin-update-checker library to new version

**version 3.0.18.4**

- Release affiliate program

**version 3.0.18.3**

- Improve logic when migrating whitelist roles options

**version 3.0.18.2**

- Fix 404 page migration issues

**version 3.0.18.1**

- Add example for API

**version 3.0.18**

- Add API documentation to developers using phpDoc 

**version 3.0.17**

- Add quick tour guide

- Add hook to track user's click

- Fix UI bug for PDA Table

- Add hook for Woo setting page

**version 3.0.16.9**

- Fix css missing for metabox in the attachment screen

**version 3.0.16.7**

- Remove expired license checking

**version 3.0.16.6**

- Hot fix for checking license expired, won't parse string to json. Compare directly string to string

**version 3.0.16.5**

- Add quick guide tour

- Add hook to track user's click

- Fix UI bug for PDA Table

**version 3.0.16.5**

- Remove custom body class to prevent the conflict with the popular plugin

**version 3.0.16.4**

- Fix css conflict with the famous plugins: Ninja Form

- Only add our body class for pages and post screen

**version 3.0.16**

- Apply fatal error log

- Add more settings

**version 3.0.15.3**

- Remove array from class constant to support PHP version < 5.6

**version 3.0.15.2**

- Resolve conflict for css class .slider 

**version 3.0.11**

- Add option that user can delete all related data and license data

- Add option that user can use raw redirect links without using .htaccess

- Delete the backup data when un-protect file

- Refactor the setting page code

**version 3.0.10**

- Add status panel for the debugging purpose

- Add UI to show which media file is protected in Media popup

- Optimize the javascript loading 

**version 3.0.9.5**

- Delete all options related to licenses

**version 3.0.9.4**

- Get attachment url by wp_get_attachment_url function instead of guid

**version 3.0.9.2**

- Fix mirror logic to check whether plugin is fully activated

- Add priority to add custom column filters

**version 3.0.9**

- Support membership options inside the individual files

**version 3.0.8.6**

- Improve UI for membership integration UI

**version 3.0.8.5**

- Clear buffer after printing the content

**version 3.0.8.4**

- Hotfix that video can be streamed without the protect videos plugin.

**version 3.0.8**

- Add UI to protect file when adding media under pages/posts

- Add option to turn on/off Block Access to WordPress Information Files and Hide WordPress Version setting

- Add filters in Media Page that can filter protected, un-protected and all files

- Add guides under helpers for Microsoft ISS server

**version 3.0.7**

- Apply automation test when deploying to production and staging

- Show guides in helpers tab in setting if user is using nginx server

- Flush rewrite rules when plugin finished updating

- Add AWS S3 tab if pda-s3 add on is activated

- Integrate with the membership plugin

**version 3.0.6.1**

- Add file access permission in per files popup

**version 3.0.6**

- Block access to license.txt & WP version

- Server video files with partial content if PDA video add on is activated

- Add AWS S3 tab in PDA Gold v3 setting page if pda s3 plugin is activated (https://app.asana.com/0/383837169626397/708823256070198)

- Add filers in Media Page that can  filters with these criteria Protected files, Non protected Files (https://app.asana.com/0/383837169626397/705037056648373)
 
**version 3.0.5**

- Integrate with pda-s3

- Fix robots.txt rules can work on sub directory

**version 3.0.4**

- Fix wrong notification when user do not have data to migrate 

**version 3.0.3.4**

- Fix disable directory list not working

**version 3.0.3.3**

- Fix home_url cannot return https schema (https://app.asana.com/0/383837169626397/705146352884933)

**version 3.0.3.2**

- Improve UI for "File Permission tab" (https://app.asana.com/0/383837169626397/699511369752365)

- Only show default private link and icon when the magic link add-on is activated (https://app.asana.com/0/383837169626397/701217000008811)

- Only 15-sites and Unlimited-site can run on multi-site mode without pda multi-site (https://app.asana.com/0/383837169626397/703583826756658) 

- Integrate User Access Manager add-on (https://app.asana.com/0/383837169626397/701217000008815)

- Fix the bug: The file has not been protected but it still shows the stats in the popup (https://app.asana.com/0/383837169626397/703583826756670)

**version 3.0.3**

- Fix logic to get no access page title 

**version 3.0.2**

- Integrate with IP Block, grant user roles for the specific file

**version 3.0.1**

- Only show helper tab in settings when enabled remote log

- If it's multi-site and license is not unlimited, need to install pda-multisite add-on

- Fix guides in helpers when it's multi-site

- Integrate with pda-magiclink, ip-block and pda-multisite add-on 

**version 3.0.0**

- Protect the file by copying to _pda folders

- Improve the performance

- Remove protection for the file

- Auto-generate a private link

- Create a custom private link

- Delete a private link

- Activate/deactivate a private link

- Count clicks on the private link

- Can set download limit and download validity

- Can copy the private link's url

- Show default place holder image if it's protected

- Can set/un-set protection in attachment's edit mode

- Add no-index meta tag to the page belonged to protected attachment

- Integrate with the IP Block add on


