<?php

/**
 * Provide a public-facing views for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://preventdirectaccess.com/extensions/?utm_source=user-website&utm_medium=pluginsite_link&utm_campaign=pda_gold
 * @since      1.0.0
 *
 * @package    Prevent_Direct_Access_Gold
 * @subpackage Prevent_Direct_Access_Gold/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
