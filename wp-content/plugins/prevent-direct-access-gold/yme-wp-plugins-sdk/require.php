<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 3/7/18
 * Time: 10:52
 */

require_once 'includes/yme_addon.php';
require_once 'includes/yme_plugin.php';
require_once 'includes/yme_htaccess.php';
require_once 'includes/yme_plugin_utils.php';
require_once 'includes/yme_aws_api.php';
require_once 'includes/class/class_yme_messages.php';
require_once 'includes/yme_license.php';
require_once 'includes/yme_aws_api_v2.php';