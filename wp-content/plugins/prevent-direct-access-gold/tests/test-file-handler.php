<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 5/17/18
 * Time: 11:18
 */

class FileHandlerTest extends WP_UnitTestCase {

    function setup() {
        parent::setup();

        $this->attachment_id = $this->_uploadAttachmentFile();
        $this->protected_dir = '_pda';
        require_once dirname( __FILE__ ) . '/../includes/class-prevent-direct-access-gold-file-handler.php';
    }

    function testIsNotAttachmentFileThrowException() {
        $exception = Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected(-1, $this->protected_dir);
        $this->assertTrue( is_wp_error($exception) );
    }

    function testProtectedDirIsAbsolutePath() {
        $exception = Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected($this->attachment_id, __DIR__ );
        $this->assertTrue( is_wp_error($exception) );

    }

    function testProtectedDirIsOldDir() {
        $result = Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected($this->attachment_id, '2018/05' );
        $this->assertTrue( $result );
    }

    function testMoveAttachmentToProtected() {
        $result = Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected($this->attachment_id, $this->protected_dir);
        $this->assertTrue($result);
    }

    function testGetFilesFromMeta() {
        $meta = array(
                array(
                    'file' => 'sample-test-120x120.jpg'
                )
        );
        $files = Prevent_Direct_Access_Gold_File_Handler::get_files_from_meta($meta);
        $this->assertEquals('sample-test-120x120.jpg', $files[0]);
    }

    function _uploadAttachmentFile() {
        $filename = dirname( __FILE__ ) . '/assets/sample-attachment.jpg';
        $contents = file_get_contents( $filename );

        $upload = wp_upload_bits( basename( $filename), null, $contents);
        $this->assertTrue( empty($upload['error']) );

        $id = $this->_make_attachment($upload);

        return $id;
    }

    function _make_attachment( $upload, $parent_post_id = 0 ) {

        $type = '';
        if ( !empty($upload['type']) ) {
            $type = $upload['type'];
        } else {
            $mime = wp_check_filetype( $upload['file'] );
            if ($mime)
                $type = $mime['type'];
        }

        $attachment = array(
            'post_title' => basename( $upload['file'] ),
            'post_content' => '',
            'post_type' => 'attachment',
            'post_parent' => $parent_post_id,
            'post_mime_type' => $type,
            'guid' => $upload[ 'url' ],
        );

        // Save the data
        $id = wp_insert_attachment( $attachment, $upload[ 'file' ], $parent_post_id );
        wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $upload['file'] ) );

        return $id;

    }
}