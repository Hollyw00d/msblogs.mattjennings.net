<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 9/18/18
 * Time: 09:49
 */

/**
 * Cron job handler class
 * Class PDA_Cronjob_Handler
 */
class PDA_Cronjob_Handler {
	
	private $logger;
	/**
	 * PDA_Cronjob_Handler constructor.
	 */
	public function __construct() {
		$this->logger     = new PDA_Logger();
	}
	
	/**
	 * Exec function for PDA license cron job
	 */
	public function pda_ls_cron_exec() {
		$service = new PDA_Services();
		$license_info = $service->get_license_info();
		if ( time() > $license_info->expired_date ) {
			$license = get_option( PDA_v3_Constants::LICENSE_KEY );
			$result = YME_LICENSE::checkExpiredLicense( $license );
			if( true === $result ) {
				Pda_Gold_Functions::update_license_expired();
			}
			$this->logger->info( 'Check license\'s response: ' . wp_json_encode( $result ) );
		}
	}

    /**
     * Exec function for PDA delete expired private link cron job
     */
    public function pda_delete_expired_private_links_cron_exec() {
        $repository = new PDA_v3_Gold_Repository();
        $repository->delete_all_private_link_expired_with_type(PDA_v3_Constants::PDA_PRIVATE_LINK_EXPIRED, PDA_v3_Constants::RETENTION_TIMES );
        $this->logger->info( 'Delete expired private links\'s response: Deleted' );
    }

	/**
	 * Schedule license cron job
	 */
	public function schedule_ls_cron_job( $force = false ) {
		if ( $force ) {
			wp_schedule_event(time(), 'quarterly', PDA_v3_Constants::PDA_LS_CRON_JOB_NAME);
		} else {
			if (! wp_next_scheduled( PDA_v3_Constants::PDA_LS_CRON_JOB_NAME ) ) {
				wp_schedule_event(time(), 'quarterly', PDA_v3_Constants::PDA_LS_CRON_JOB_NAME);
			}
		}
	}

    /**
     * Schedule delete expired private link cron job
     */
    public function schedule_delete_expired_private_links_cron_job() {
	    if ( Yme_Plugin_Utils::is_plugin_activated( 'wp_protect_password' ) !== -1 ) {
	    	return false;
	    }
        if (! wp_next_scheduled( PDA_v3_Constants::PDA_DELETE_EXPIRED_PRIVATE_LINK_CRON_JOB_NAME ) ) {
            wp_schedule_event(time(), 'daily', PDA_v3_Constants::PDA_DELETE_EXPIRED_PRIVATE_LINK_CRON_JOB_NAME);
        }
    }
	
	/**
	 * Un-schedule license cron job
	 */
	public function unschedule_ls_cron_job() {
		$timestamp = wp_next_scheduled( PDA_v3_Constants::PDA_LS_CRON_JOB_NAME );
		wp_unschedule_event( $timestamp, PDA_v3_Constants::PDA_LS_CRON_JOB_NAME );
	}

    /**
     * Un-schedule delete expired private link cron job
     */
    public function unchedule_delete_expired_private_links_cron_job() {
        $timestamp = wp_next_scheduled( PDA_v3_Constants::PDA_DELETE_EXPIRED_PRIVATE_LINK_CRON_JOB_NAME );
        wp_unschedule_event( $timestamp, PDA_v3_Constants::PDA_DELETE_EXPIRED_PRIVATE_LINK_CRON_JOB_NAME );
    }
	
	/**
	 * Add our custom intervals
	 *
	 * @param $schedules
	 *
	 * @return mixed
	 */
	public function add_custom_intervals( $schedules) {
		$one_month = 2635200;
		$schedules['quarterly'] = array(
			'interval' => $one_month * 4,
			'display' => __('Once Quarterly')
		);
		return $schedules;
	}
}