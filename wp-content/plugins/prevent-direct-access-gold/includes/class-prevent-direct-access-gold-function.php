<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have sufficient permissions to access this file.' );
}

/**
 * Class Pda_Gold_Functions
 */
class Pda_Gold_Functions {
	/**
	 * @param $key
	 *
	 * @return mixed
	 */
	public static function tooltip_content( $key ) {
		$contents = array(
			'enable_remote_logs'                  => 'Allow us to get necessary data that makes troubleshooting easier when something goes wrong. No sensitive data is tracked.',
			'grant_access_to_all_logged-in_users' => 'All logged-in users including admins and subscribers will be able to access your protected files.',
			'grant_access_to_these_roles_only'    => 'Only allow the following user roles to access your protected files.',
			'private_url_prefix'                  => 'Change “private”  prefix of your download link.',
			'auto-protect_future_uploaded_files'  => 'Prevent Direct Access will automatically protect all your future uploaded files.',
			'prevent_hotlinking'                  => 'Stop unwanted users from stealing and using your images and files without permission.',
			'pda_gold_enable_directory_listing'   => 'Disable directory browsing of all folders and subdirectories.',
			'pda_gold_no_access_page'             => 'Select which page to display when users have no access to your protected files.',
			'file_access_permission'              => 'Select user roles who can access your protected files.',
			'hiden_wp_version'                    => 'Removing WordPress generator meta tag.',
			'block_access_files'                  => 'Blocking access to readme.html, license.txt and wp-config-sample.php',
//             'pda_enable_protection' => "While this option blocks Google from indexing your protected files through robots.txt, it doesn't block their original links, which are still accessible by anyone."
			'home_url'                            => 'The homepage URL of your site.',
			'site_url'                            => 'The root URL of your site.',
			'version'                             => 'The version of Prevent Direct Access Gold installed on your site.',
			'wp_version'                          => 'The version of WordPress installed on your site.',
			'wp_multisite'                        => 'Whether or not you have WordPress Multisite enabled.',
			'wp_memory_limit'                     => 'The maximum amount of memory (RAM) that your site can use at one time.',
			'wp_debug_mode'                       => 'Displays whether or not WordPress is in Debug Mode.',
			'wp_cron'                             => 'Displays whether or not WP Cron Jobs are enabled.',
			'language'                            => 'The current language used by WordPress. Default = English',
			'external_object_cache'               => 'Displays whether or not WordPress is using an external object cache.',
			'server_info'                         => 'Information about the web server that is currently hosting your site.',
			'php_version'                         => 'The version of PHP installed on your hosting server.',
			'php_post_max_size'                   => 'The largest filesize that can be contained in one post.',
			'php_max_execution_time'              => 'The amount of time (in seconds) that your site will spend on a single operation before timing out (to avoid server lockups).',
			'php_max_input_vars'                  => 'The maximum number of variables your server can use for a single function to avoid overloads.',
			'curl_version'                        => 'The version of cURL installed on your server.',
			'suhosin_installed'                   => 'Suhosin is an advanced protection system for PHP installations. It was designed to protect your servers on the one hand against a number of well known problems in PHP applications and on the other hand against potential unknown vulnerabilities within these applications or the PHP core itself. If enabled on your server, Suhosin may need to be configured to increase its data submission limits.',
			'mysql_version'                       => 'The version of MySQL installed on your hosting server.',
			'max_upload_size'                     => 'The largest filesize that can be uploaded to your WordPress installation.',
			'default_timezone'                    => 'The default timezone for your server.',
			'fsockopen_or_curl_enabled'           => 'Payment gateways can use cURL to communicate with remote servers to authorize payments, other plugins may also use it when communicating with remote services.',
			'soapclient_enabled'                  => 'Some webservices like shipping use SOAP to get information from remote servers, for example, live shipping quotes from FedEx require SOAP to be installed.',
			'domdocument_enabled'                 => 'HTML/Multipart emails use DOMDocument to generate inline CSS in templates.',
			'gzip_enabled'                        => 'GZip (gzopen) is used to open the GEOIP database from MaxMind.',
			'mbstring_enabled'                    => 'Multibyte String (mbstring) is used to convert character encoding, like for emails or converting characters to lowercase.',
			'remote_post_successful'              => 'PayPal uses this method of communicating when sending back transaction information.',
			'remote_get_successful'               => 'Prevetn Direct Access Gold plugins may use this method of communication when checking for plugin updates.',
			'pda_database_version'                => 'The version of Prevent Direct Access Gold that the database is formatted for. This should be the same as your Prevent Direct Access Gold version.',
			'database_prefix'                     => 'Prefix of table in Wordpress',
			'secure_connection'                   => 'Is the connection to your store secure?',
			'hide_errors'                         => 'Error messages can contain sensitive information about your store environment. These should be hidden from untrusted visitors.',
			'name_theme'                          => 'The name of the current active theme.',
			'version_theme'                       => 'The installed version of the current active theme.',
			'author_url_theme'                    => 'The theme developers URL.',
			'is_child_theme_theme'                => 'Displays whether or not the current theme is a child theme.',
			'pda_log_writable'                    => 'The PDA log directory',
			'pda_gold_remove_license'             => 'If this option is checked, your license and ALL related data will be removed from the database upon uninstall. Your license may NOT be used on this website again or elsewhere anymore.',
			'use_redirect_urls'                   => 'You should enable this option ONLY when you\'re using WordPress.com or Ngnix hostings that don\'t allow rewrite rules modification',
			'auto-create-new-private-link'        => 'Auto-generate a private download link after a file is protected',
			'auto-replace-protected-file'         => 'Auto-replace unprotected file URLs already embedded in the following posts and pages',
			'auto-replace-pages-posts'            => 'Only replace unprotected file URLs on these pages or posts',
            'force-download'                      => 'Force download',
            'activate_all_sites'                  => 'Activate main site\'s license on existing subsites',
		    'count_activated_for_multisite'       => 'The number of new subsites in which the main license is activated'
        );

		return $contents[ $key ];
	}

	/**
	 * @param $name
	 * @param $value
	 */
	public static function update_site_settings( $name, $value ) {
		$options = get_site_option( PDA_v3_Constants::SITE_OPTION_NAME, false );
		if ( $options === false ) {
			$options = array();
		} else {
			$options = serialize( $options );
		}
		$options[ $name ] = $value;
		update_site_option( PDA_v3_Constants::SITE_OPTION_NAME, serialize( $options ) );
	}

	/**
	 * @param bool $is_deactivate
	 */
	public static function plugin_clean_up( $is_deactivate = false ) {
		remove_filter( 'mod_rewrite_rules', 'Prevent_Direct_Access_Gold_Htaccess::pda_handle_htaccess_rewrite_rules', 9999, 2 );
		flush_rewrite_rules();

		$cronjob_handler = new PDA_Cronjob_Handler();
		$cronjob_handler->unschedule_ls_cron_job();
		$cronjob_handler->unchedule_delete_expired_private_links_cron_job();

		$timestamp = wp_next_scheduled( 'pda_cleanup_logs' );
		wp_unschedule_event( $timestamp, 'pda_cleanup_logs' );

		if ( ! $is_deactivate ) {
			$function = new Pda_Gold_Functions();
			$function->delete_license_and_all_data();
			delete_option( PDA_v3_Constants::MIGRATE_DATA );
			delete_option( PDA_v3_Constants::OPTION_NAME );
			delete_site_option( PDA_v3_Constants::FULLY_ACTIVATED );
			delete_site_option( PDA_v3_Constants::SITE_OPTION_NAME );
			delete_option( PDA_v3_Constants::$db_version );
			delete_site_option( PDA_v3_Constants::APP_ID );
		}
	}

	/**
	 *
	 */
	public static function move_files_after_deactivate(){
		$function = new Pda_Gold_Functions();
		$status_files = $function->get_status_move_files();
		$total_files = $status_files['total_files'];
		if ( ! empty( $total_files ) && intval( $total_files) >= PDA_v3_Constants::PDA_MAX_VALUE_MOVE_FILES ) {
			if ( $function->is_move_files_after_activate_async() ) {
				wp_die( '<pre>We’re handling ' . $status_files['num_of_protected_files'] . '/' . $status_files['total_files'] . ' protected files. Please come back in a while.<br><a href="' . get_admin_url() . '">Click here</a> to go back to your admin dashboard.
			</pre>' );
			}
			$move_files_after_deactivate = new PDA_Move_Files_After_Deactivate();
			$move_files_after_deactivate->push_to_queue( array() );
			$move_files_after_deactivate->save()->dispatch();
		} else {
			$repository = new PDA_v3_Gold_Repository();
			$repository->un_protect_files();
		}
	}

	/**
	 * @param $message
	 */
	public static function write_debug_log( $message ) {
		$logger = new PDA_Logger();
		$logger->info( $message );

	}

	/**
	 *
	 */
	public function delete_license_and_all_data() {
		$setting = new Pda_Gold_Functions();
		if ( $setting->get_site_settings( PDA_v3_Constants::REMOVE_LICENSE_AND_ALL_DATA ) ) {
			//Delete license
			delete_site_option( PDA_v3_Constants::LICENSE_OPTIONS );
			delete_site_option( PDA_v3_Constants::LICENSE_KEY );
			delete_site_option( PDA_v3_Constants::LICENSE_ERROR );

			//Drop table
			global $wpdb;
			$table_pda = $wpdb->prefix . 'prevent_direct_access';
			$wpdb->query( "DROP TABLE IF EXISTS $table_pda" );

			//Delete all file backup
			$table_postmeta = $wpdb->prefix . "postmeta";
			$wpdb->delete( $table_postmeta, array(
				'meta_key'   => '_pda_protection',
				'meta_value' => 1
			) );
		}
	}

	/**
	 * prams : $nameSettings
	 * return bool
	 */

	public function get_site_settings( $nameSettings ) {
		$settings = get_site_option( PDA_v3_Constants::SITE_OPTION_NAME );

		return $this->check_option_settings( $settings, $nameSettings );
	}

	/**
	 * @param $settings
	 * @param $nameSettings
	 *
	 * @return bool
	 */
	public function check_option_settings( $settings, $nameSettings ) {
		if ( $settings ) {
			$options = unserialize( $settings );
			if ( is_null( $options ) ) {
				return false;
			}
			if ( array_key_exists( $nameSettings, $options ) && ( $options[ $nameSettings ] == "true" || $options[ $nameSettings ] == "1" ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return string
	 */
	public static function get_license_type() {
		$app_id = get_site_option( PDA_v3_Constants::APP_ID, null );
		if ( is_null( $app_id ) ) {
			return "N/A";
		}
		switch ( $app_id ) {
			case "583147":
				return "1-site license";
				break;
			case "584087":
				return "3-site license";
				break;
			case "77844608":
				return "15-site license";
				break;
			case "584088":
				return "Unlimited-site license";
				break;
			case "77814469":
				return "Developer license";
				break;
			default:
				return "N/A";
				break;
		}
	}

	/**
	 * @return bool
	 */
	static function is_data_migrated() {
		return get_option( PDA_v3_Constants::MIGRATE_DATA, false ) === '1';
    }

	/**
	 * @return bool
	 */
	static function is_fully_activated() {
		return ! ! get_site_option( PDA_v3_Constants::FULLY_ACTIVATED, false );
	}

	/**
	 *
	 */
	public static function fully_activated() {
		update_site_option( PDA_v3_Constants::FULLY_ACTIVATED, true );
	}

	/**
	 * @return bool
	 */
	public static function is_v2_installed() {
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		$plugins      = get_plugins();
		$is_installed = false;

		foreach ( $plugins as $plugin_path => $plugin ) {
			if ( $plugin['Name'] === 'Prevent Direct Access Gold' ) {
				$version = (int) explode( '.', $plugin['Version'] )[0];
				if ( $version < 3 ) {
					$is_installed = true;
					break;
				}
			}
		}

		return $is_installed;
	}

	/**
	 * @return bool
	 */
	public static function check_unlimited_license() {
		$app_id = get_site_option( PDA_v3_Constants::APP_ID, null );

		return PDA_v3_Constants::UN_LIMITED_LICENSE === $app_id || PDA_v3_Constants::FIFTEEN_SITE_LICENSE === $app_id || PDA_v3_Constants::FIFTEEN_SITE_LIFETIME_LICENSE === $app_id;
	}

	/**
	 * @return mixed|PDA_Logger|void|null
	 */
	public static function pda_get_logger() {
		static $logger = null;

		$class = apply_filters( 'pda_logging_class', 'PDA_Logger' );

		if ( null === $logger || ! is_a( $logger, $class ) ) {
			$implements = class_implements( $class );

			if ( is_array( $implements ) && in_array( 'PDA_Logger_Interface', $implements, true ) ) {
				if ( is_object( $class ) ) {
					$logger = $class;
				} else {
					$logger = new $class();
				}
			} else {
				$logger = is_a( $logger, 'PDA_Logger' ) ? $logger : new PDA_Logger();
			}
		}

		return $logger;
	}

	/**
	 *
	 */
	public function set_default_settings() {
		$this->set_migration_or_not();

		// Set default setting for file access permission
		$pda_service = new PDA_Services();
		$pda_service->pda_gold_set_default_setting_for_fap();
		$pda_service->pda_gold_set_default_setting_for_role_protection();
	}

	/**
	 *
	 */
	public function set_migration_or_not() {
		if ( get_option( PDA_v3_Constants::LICENSE_OPTIONS, null ) !== "1" ) {
			update_option( PDA_v3_Constants::MIGRATE_DATA, true, 'no' );
		}
	}

	/**
	 * @param $name
	 *
	 * @return string
	 */
	public function prefix_roles_name( $name ) {
		$settings = get_site_option( PDA_v3_Constants::SITE_OPTION_NAME );
		if ( $settings ) {
			$options        = unserialize( $settings );
			$selected_roles = array_key_exists( $name, $options ) ? $options[ $name ] : '';
			if ( $selected_roles != '' ) {
				return $selected_roles;
			}
		}

		return PDA_v3_Constants::$default_private_link_prefix;
	}

	/**
	 * @return array
	 */
	public function getPdaApplyLogged() {
		$is_logged = $this->getSettings( 'pda_apply_logged_user' );
		$whitelist_user_groups = $this->getSettings( 'whitelist_user_groups' );
		if ( $is_logged == false ) {
			$value = 'blank';
			if( ! empty( $whitelist_user_groups ) ) {
				$value = 'custom_roles';
			}
		} else {
			$value = 'logged_users';
		}
		$file_access_permission = [ 'file_access_permission' => $value ];
		return $file_access_permission;
	}

	/**
	 * @param $nameSettings
	 *
	 * @return bool
	 */
	public function getSettings( $nameSettings ) {
		$settings = get_option( PDA_v3_Constants::OPTION_NAME );

		return $this->check_option_settings( $settings, $nameSettings );
	}

	/**
	 * @return string|void
	 */
	public function get_domain_info() {
		$schema = "http";
		if ( is_ssl() ) {
			$schema = "https";
		}
		return home_url( '/', $schema );
	}

	/**
	 * @param $attachment_id
	 *
	 * @return bool
	 */
	public function check_roles_with_whitelist( $attachment_id ) {
		$option = $this->selected_roles( PDA_v3_Constants::FILE_ACCESS_PERMISSION );
		self::write_debug_log( sprintf( 'File access permissions option: %s', $option ) );
		switch ( $option ) {
			case "custom_roles":
				$role = $this->checkUserLoginByRoles();
				if ( $role ) {
					return true;
				}
				break;
			case "admin_users":
				if ( current_user_can( 'manage_options' ) ) {
					return true;
				}
				break;
			case "author":
				if ( get_current_user_id() == get_post_field( 'post_author', $attachment_id, 'raw' ) ) {
					return true;
				}
				break;
			case "logged_users":
				if ( is_user_logged_in() ) {
					return true;
				}
				break;
			case "anyone":
				return true;
				break;
			default:
				return false;
				break;
		}

		return false;
	}

	/**
	 * prams : $name
	 * return array
	 */
	public function selected_roles( $name ) {
		$settings = get_option( PDA_v3_Constants::OPTION_NAME );
		if ( $settings ) {
			$options        = unserialize( $settings );
			$selected_roles = array_key_exists( $name, $options ) ? $options[ $name ] : '';
			if ( $selected_roles != '' ) {
				return $selected_roles;
			} else {
				return array();
			}
		}

		return array();
	}

	/**
	 * @return bool
	 */
	public function checkUserLoginByRoles() {
		$user_login = wp_get_current_user()->roles;
		self::write_debug_log( sprintf( 'User roles: %s', wp_json_encode( $user_login) ) );
		$user_roles = $this->selected_roles( PDA_v3_Constants::WHITElIST_ROLES );
		self::write_debug_log( sprintf( 'Whitelist roles: %s', wp_json_encode( $user_roles) ) );
		if ( ! empty( array_intersect( $user_login, $user_roles ) ) ) {
			return true;
		} else if ( is_super_admin( wp_get_current_user()->ID ) ) {
			if ( in_array( "administrator", $user_roles ) ) {
                return true;
            }
		} else {
            return false;
        }
	}

	/**
	 * @return bool
	 */
	public static function is_license_expired() {
		if ( get_option( PDA_v3_Constants::LICENSE_EXPIRED ) === '1' ) {
			return true;
		}
		return false;
	}

	/**
	 * Update expired license.
	 *
	 * @return mixed
	 */
	public static function update_license_expired() {
		error_log( 'License has been expired!' );
		self::write_debug_log( 'License has been expired.! Updating...' );
		return update_option( PDA_v3_Constants::LICENSE_EXPIRED, true, false );
	}

	/**
	 * @param $post_id
	 *
	 * @return bool
	 */
	public function check_file_synced_s3($post_id) {
        $metadata = get_post_meta( $post_id, PDA_v3_Constants::PDA_S3_LINK_META, true );
        return ($metadata !== false && ! empty( $metadata )) ? true : false;
    }

	/**
	 * @param $plugin_basename
	 */
	public function delete_pda_free_version( $plugin_basename ) {
        $dependent = ( 'prevent-direct-access/prevent-direct-access.php' );
        if ( is_plugin_active( $dependent ) ) {
            deactivate_plugins( $plugin_basename );
            wp_die( YME_MESSAGES::$PDA['PDA_NOTIFICATION_ACTIVATE'] );
        }
        delete_plugins( [ $dependent ] );
    }

	/**
	 * @param $attachment_id
	 *
	 * @return bool
	 */
	public function check_user_access_for_ip_block_addon( $attachment_id ){
        require_once ABSPATH . 'wp-content/plugins/wp-pda-ip-block/admin/class-wp-pda-ip-block-admin.php';
        $data = Wp_Pda_Ip_Block_Admin::get_ip_block_by_post_id( $attachment_id );
        if ( empty( $data->user_roles ) || 'default' === unserialize( $data->user_roles )['type'] ) {
	        return $this->check_roles_with_whitelist( $attachment_id );
        } else {
	        $user_roles = unserialize( $data->user_roles );
	        $type       = $user_roles["type"];
	        return $this->check_fap_for_ip_block( $attachment_id, $user_roles, $type );
        }
    }


	/**
	 * @param $attachment_id
	 *
	 * @return bool
	 */
	public function check_user_access_normal_way ( $attachment_id ){
        $type = get_post_meta( $attachment_id, PDA_v3_Constants::$pda_meta_key_user_roles, true );
        if ( empty( $type ) || 'default' === $type ) {
	        return $this->check_roles_with_whitelist( $attachment_id );
        } else {
            switch ( $type ) {
                case "admin-user":
                    if ( current_user_can( 'manage_options' ) ) {
                        return true;
                    }
                    break;
                case "author":
                    if ( get_current_user_id() == get_post_field( 'post_author', $attachment_id, 'raw' ) ) {
                        return true;
                    }
                    break;
                default:
                    return false;
            }
        }

        return false;
    }

	/**
	 * @param $attachment_id
	 *
	 * @return bool
	 */
	public function check_file_access_permission_for_post( $attachment_id ){
		if ( Yme_Plugin_Utils::is_plugin_activated( 'membership' ) === - 1 ) {
			$check_memberships = $this->handle_access_membership_integration( $attachment_id );
			if ( $check_memberships ) {
				return true;
			}
		}

        if ( Yme_Plugin_Utils::is_plugin_activated( 'ip_block' ) === - 1 ) {
	        return $this->check_user_access_for_ip_block_addon( $attachment_id );
        }

        return $this->check_user_access_normal_way( $attachment_id );
    }

	/**
	 * @param $attachment_id
	 * @param $user_roles
	 * @param $type
	 *
	 * @return bool
	 */
	public function check_fap_for_ip_block( $attachment_id, $user_roles, $type ) {
		switch ( $type ) {
			case "custom-roles":
				if ( ! array_key_exists( 'roles', $user_roles ) ) {
					break;
				}
				$roles = explode( ";", $user_roles["roles"] );
				$user_Login = wp_get_current_user()->roles;
				if ( ! empty( array_intersect( $user_Login, $roles ) ) ) {
					return true;
				} else if ( is_super_admin( wp_get_current_user()->ID ) ) {
					if ( in_array( "administrator", $roles ) ) {
						return true;
					}
				}
				break;

			case "admin-user":
				if ( current_user_can( 'manage_options' ) ) {
					return true;
				}
				break;
			case "author":
				if ( get_current_user_id() == get_post_field( 'post_author', $attachment_id, 'raw' ) ) {
					return true;
				}
				break;
			case "logger-in-user":
				if ( is_user_logged_in() ) {
					return true;
				}
				break;
			case "anyone":
				return true;
			case "blank":
				return false;
			default:
				return false;
		}

		return false;
	}

	/**
	 * @param $attachment_id
	 *
	 * @return bool
	 */
	public function handle_access_membership_integration( $attachment_id ) {
		if ( class_exists( 'Pda_Membership_Integration_Admin' ) ) {
			$pda_admin = new Pda_Membership_Integration_Admin( "", "" );
			return $pda_admin->pda_check_access_for_membership_integration( $attachment_id );
		}

		return false;
	}

	/**
	 * @param $content
	 *
	 * @return mixed
	 */
	public function find_and_replace_private_link_for_dflip( $content ) {
        // Replace private link for Dflip plugin
        $content = $this->replace_private_url_for_type_pdf_dflip_plugin( $content );
        $content = $this->replace_private_url_for_thumbnail_dflip_plugin( $content );
        return $content;
    }

	/**
	 * @param $content
	 *
	 * @return mixed
	 */
	function replace_private_url_for_type_pdf_dflip_plugin( $content ) {
        $elements = array();
        $search = '\"source\":\"([^\"]*)\"';
        preg_match_all( "/$search/iU", $content, $elements, PREG_PATTERN_ORDER );
        $url_file = array_unique( $elements[1] );

        return $this->pda_v3_handle_content_before_show_ui( $url_file, $content, 'source' );
    }

	/**
	 * @param $content
	 *
	 * @return mixed
	 */
	function replace_private_url_for_thumbnail_dflip_plugin( $content ) {
        $thumb = array();
        $search_thumb = '<div\s[^>]*?thumb\s*=\s*[\'\"]([^\'\"]*?)[\'\"][^>]*?>';
        preg_match_all( "/$search_thumb/iU", $content, $thumb, PREG_PATTERN_ORDER );
        $thumb_link = array_unique( $thumb[1] );

        return $this->pda_v3_handle_content_before_show_ui( $thumb_link, $content, 'thumb' );
    }

	/**
	 * @param $url_file
	 * @param $content
	 * @param $type
	 *
	 * @return mixed
	 */
	function pda_v3_handle_content_before_show_ui( $url_file, $content, $type ) {
        foreach ( $url_file as $file ) {
            $massaged_file = preg_replace( '/^(\S+)-[0-9]{1,4}x[0-9]{1,4}(\.[a-zA-Z0-9\.]{2,})?/', '$1$2', $file );
            if ( $type === 'source' ) {
                $massaged_file = str_replace("\\", "", $massaged_file );
            }
            $post_id = $this->pda_v3_get_post_id_by_original_link( $massaged_file );
            $repo_v3 = new PDA_v3_Gold_Repository();
            if ( $repo_v3->is_protected_file( $post_id ) ) {
                if ( ! $this->check_file_access_permission_for_post( $post_id ) ) {
                    $new_private_link = $this->create_private_link_for_dflip( $post_id );
                    $private_link = Pda_v3_Gold_Helper::get_private_url( $new_private_link );
                    if ( $type === 'source' ) {
                        $content = str_replace( '"' . $type . '":"' . $file . '"', '"' . $type . '":"' . $private_link . '"', $content );
                    } else {
                        $content = str_replace( $type . '="' . $file . '"', $type . '="' . $private_link . '"', $content );
                    }
                }
            }
        }

        return $content;
    }

	/**
	 * @param $post_id
	 *
	 * @return string
	 */
	function create_private_link_for_dflip( $post_id ) {
        $repo_v3 = new PDA_v3_Gold_Repository();
        $private_link = Pda_v3_Gold_Helper::generate_unique_string();
        $repo_v3->create_private_link( array(
            'post_id'         => $post_id,
            'is_prevented'    => true,
            'limit_downloads' => 1,
            'url'             => $private_link,
            'type'            => 'p_expired'
        ) );
        return $private_link;
    }

	/**
	 * @param $link
	 *
	 * @return bool|int
	 */
	function pda_v3_get_post_id_by_original_link( $link ) {
        $wp_upload_dir  = wp_upload_dir();
        $baseurl        = $wp_upload_dir['baseurl'];
        $meta_value     = str_replace( $baseurl . '/', '', $link );
        $arr_meta_value = explode( '/', $meta_value );
        $repo_v3           = new PDA_v3_Gold_Repository();
        if ( ! in_array( '_pda', $arr_meta_value, true ) ) {
            array_unshift( $arr_meta_value, '_pda' );
        }
        $meta_value = implode( '/', $arr_meta_value );
        $post_id    = $repo_v3->get_post_id_by_meta_value( '_wp_attached_file', $meta_value );
        if ( false !== $post_id ) {
            return $post_id;
        }
        $meta_value = str_replace( '_pda/', '', $meta_value );

        return $repo_v3->get_post_id_by_meta_value( '_wp_attached_file', $meta_value );
    }

	/**
	 * @param $id
	 * @param $activate_data
	 *
	 * @return bool
	 */public function activate_site_by_id( $id, $activate_data ){
	    if ( !get_blog_option($id, 'pda_license_key') ){
            foreach ( array_keys($activate_data) as $key){
                update_blog_option($id, $key, $activate_data[$key] );
                $this->set_default_settings_for_site( $id );
            }
            return true;
        }
	    return false;
    }

	/**
	 * @return int
	 */
	public function activate_all_sites() {
	    if ( ! class_exists('Yme_AWS_Api_v2') )
	        return 0;
	    $count_site_activated = 0;
	    if ( is_multisite()  ) {
	        $activate_data = array(
	            'pda_license_key' => get_option('pda_license_key'),
                'pda_is_licensed' => get_option('pda_is_licensed'),
                'pda_License_info' => get_option('pda_License_info')
            );
            $api = new Yme_AWS_Api_v2();
            if ( method_exists($api,'updateCountAndUserAgents') ) {
                $response = $api->getAvailableDomain( $activate_data['pda_license_key'] );
                if ( ! property_exists($response, 'status') && ! property_exists($response, 'num') ) {
                    return -1;
                }
                if ( $response->status === true && ( $response->num > 0 || $response->num === 'Infinity' ) ) {
                    $sites = get_sites();
                    update_option( PDA_v3_Constants::PDA_V3_ACTIVATE_ALL_SITES_OPTION_NAME, json_encode(array(
                        'num' => 0
                    )));
                    $agents = '';
                    if ( $response->num !== 'Infinity' ) {
                        $available_activate = $response->num;
                    }else {
                        $available_activate = count($sites);
                    }
                    foreach ($sites as $site) {
                        if ( $available_activate <= 0 )
                            break;
                        if ( $this->activate_site_by_id($site->blog_id, $activate_data)) {
                            $count_site_activated++;
                            $available_activate--;
                            $agents = $agents . ';' . get_blog_details($site->blog_id)->domain . get_blog_details($site->blog_id)->path;
                            update_option( PDA_v3_Constants::PDA_V3_ACTIVATE_ALL_SITES_OPTION_NAME, json_encode(array(
                                'num' => $count_site_activated
                            )));
                        }
                    }
                    if ($count_site_activated > 0) {
                        $api->updateCountAndUserAgents($activate_data['pda_license_key'], $agents, $count_site_activated);
                    }
                }
            }
        }
        update_option( PDA_v3_Constants::PDA_V3_ACTIVATE_ALL_SITES_OPTION_NAME, json_encode(array(
            'num' => $count_site_activated
        )));
        return $count_site_activated;
    }

	/**
	 * @return bool
	 */
	public function is_activate_all_sites_async(){
        $cron_array = _get_cron_array();
        if ( ! $cron_array ) {
	        return false;
        }
	    foreach ( array_keys($cron_array) as $key) {
	        if ( array_keys($cron_array[$key])[0] === 'wp_pda_activate_all_sites_cron' ) {
	            return true;
            }
        }
	    return false;
    }

	/**
	 * @param $id
	 */
	public function set_default_settings_for_site( $id ) {
		if ( is_null( get_blog_option( $id, PDA_v3_Constants::OPTION_NAME, null ) ) ) {
			$default_options = array(
				"file_access_permission" => "admin_users"
			);
			add_blog_option( $id, PDA_v3_Constants::OPTION_NAME, serialize( $default_options ), '', 'no' );
		}
	}

	/**
	 * @return bool
	 */
	public function is_move_files_after_activate_async() {
	    $cron_array = _get_cron_array();
	    if ( ! $cron_array ) {
		    return false;
	    }
	    foreach ( array_keys($cron_array) as $key) {
		    if ( array_keys($cron_array[$key])[0] === 'wp_pda_move_files_after_activate_cron' ) {
			    return true;
		    }
	    }
	    return false;
    }

	/**
	 * @return bool
	 */
	public function is_move_files_after_deactivate_async() {
		$cron_array = _get_cron_array();
		if ( ! $cron_array ) {
			return false;
		}
		foreach ( array_keys($cron_array) as $key) {
			if ( array_keys($cron_array[$key])[0] === 'wp_pda_move_files_after_deactivate_cron' ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return array
	 */
	public function get_status_move_files() {
		$repo = new PDA_v3_Gold_Repository();
		return array(
			'total_files'            => $repo->get_protected_files(),
			'num_of_protected_files' => get_option( PDA_v3_Constants::PDA_NUM_BACKUP_FILES_OPTION )
		);
	}

	/**
	 * @return bool
	 */
	public function pda_check_role_protection() {
		if ( is_super_admin( wp_get_current_user()->ID ) ) {
			return true;
		}
		$user_login = wp_get_current_user()->roles;
		foreach ( $user_login as $role ) {
			if ( 'administrator' === $role ) {
				return true;
			}
		}
		$role_access = $this->selected_roles( PDA_v3_Constants::PDA_GOLD_ROLE_PROTECTION );
		if ( ! empty( array_intersect( $user_login, $role_access ) ) ) {
			return true;
		}
		return false;
	}

}
