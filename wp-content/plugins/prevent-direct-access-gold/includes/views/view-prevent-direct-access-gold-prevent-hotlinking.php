<tr>
	<td>
		<label class="pda_switch" for="pda_gold_enable_image_hot_linking">
			<?php if ( $setting->get_site_settings( PDA_v3_Constants::PDA_GOLD_ENABLE_IMAGE_HOT_LINKING ) ) { ?>
				<input type="checkbox" id="pda_gold_enable_image_hot_linking" checked/>
			<?php } else { ?>
				<input type="checkbox" id="pda_gold_enable_image_hot_linking"/>
			<?php } ?>
				<span class="pda-slider round"></span>
		</label>
	</td>
    <td>
        <p>
            <label><?php echo esc_html__( 'Prevent Image Hotlinking', 'prevent-direct-access-gold' ) ?></label>
            <?php echo esc_html__( 'Prevent other people from stealing and using your images or files without permission', 'prevent-direct-access-gold' ) ?>
        </p>
    </td>
</tr>