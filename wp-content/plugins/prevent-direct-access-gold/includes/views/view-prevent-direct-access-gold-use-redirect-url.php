<tr>
	<td>
		<?php if ( $setting->get_site_settings( PDA_v3_Constants::USE_REDIRECT_URLS ) ) { ?>
			<label class="pda_switch" for="use_redirect_urls">
				<input type="checkbox" id="use_redirect_urls" name="use_redirect_urls" checked/>
				<span class="pda-slider round"></span>
			</label>
		<?php } else { ?>
			<label class="pda_switch" for="use_redirect_urls">
				<input type="checkbox" id="use_redirect_urls" name="use_redirect_urls"/>
				<span class="pda-slider round"></span>
			</label>
		<?php } ?>
		<div class="pda_error" id="pda_l_error"></div>
	</td>
    <td>
        <p>
            <label><?php echo esc_html__( 'Keep Raw URLs', 'prevent-direct-access-gold' ) ?></label>
            <?php echo esc_html__( 'Keep Raw URLs for both Private and Original file URLs. Enable this option ONLY when you are using Wordpress.com or NGINX hostings that don\'t allow rewrite rules modification.', 'prevent-direct-access-gold' ) ?>
        </p>
    </td>
</tr>