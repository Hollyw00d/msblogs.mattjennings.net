<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 5/17/18
 * Time: 15:18
 */

if ( ! class_exists( 'PDA_Api_Gold' ) ) {
	/**
	 * Class PDA_Api_Gold
	 */
	class PDA_Api_Gold {
		/**
		 * Database repository
		 * @var PDA_v3_Gold_Repository
		 */
		private $repo;

		/**
		 * PDA_Api_Gold constructor.
		 */
		public function __construct() {
			$this->repo = new PDA_v3_Gold_Repository();
		}

		/**
		 * Register rest routes
		 */
		public function register_rest_routes() {
			register_rest_route( 'pda/v3', '/files/(?P<id>\d+)', array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'protect_files' ),
				'permission_callback' => function () {
					return current_user_can( 'upload_files' );
				},
			) );

			register_rest_route( 'pda/v3', '/files/(?P<id>\d+)', array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'is_protected' ),
				'permission_callback' => function () {
					return current_user_can( 'upload_files' );
				},
			) );

			register_rest_route( 'pda/v3', '/un-protect-files/(?P<id>\d+)', array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'un_protect_files' ),
				'permission_callback' => function () {
					return current_user_can( 'upload_files' );
				},
			) );

			register_rest_route( 'pda/v3', '/private-urls/(?P<id>\d+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'list_private_links' ),
			) );

			register_rest_route( 'pda/v3', '/private-urls/(?P<id>\d+)', array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'create_private_urls' ),
				'permission_callback' => function () {
					return current_user_can( 'upload_files' );
				},
			) );

			register_rest_route( 'pda/v3', '/delete-private-urls/(?P<id>\d+)', array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'delete_private_urls' ),
				'permission_callback' => function () {
					return current_user_can( 'upload_files' );
				},
			) );

			register_rest_route( 'pda/v3', '/update-private-urls/(?P<id>\d+)', array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'update_private_urls' ),
				'permission_callback' => function () {
					return current_user_can( 'upload_files' );
				},
			) );

			register_rest_route( 'pda/v3', '/set-default/(?P<id>\d+)', array(
				'methods'  => 'POST',
				'callback' => array( $this, 'set_default' ),

			) );

			register_rest_route( 'pda/v3', '/remote-log', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'remoteLogHandle' ),
			) );

			register_rest_route( 'pda/v3', '/migrate-data', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'migrateData' ),
			) );

            register_rest_route( 'pda/v3', '/generate-expired-link-for-video/(?P<id>\d+)', array(
                'methods'  => 'POST',
                'callback' => array( $this, 'generate_expired_for_video' ),
            ) );

			register_rest_route( 'pda/v1', '/generate-expired-link/(?P<id>\d+)', array(
				'methods'  => 'POST',
				'callback' => array( $this, 'generateExpired' ),
			) );

			register_rest_route( 'pda/v1', '/delete-private-link', array(
				'methods'  => 'POST',
				'callback' => array( $this, 'delete_private_link' ),
			) );

			register_rest_route( 'pda/v1', '/generate-expired-from-private', array(
				'methods'  => 'POST',
				'callback' => array( $this, 'generateExpiredFromPrivateLink' ),
			) );

			//api for ip block.
			register_rest_route( 'pda-ip-block/v1', '/add-user-roles/(?P<post_id>[0-9-]+)', array(
				'methods'  => 'POST',
				'callback' => array( $this, 'add_user_roles_to_meta_post' )
			) );

			register_rest_route( 'pda-ip-block/v1', '/get-user-roles/(?P<post_id>[0-9-]+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'get_user_roles_in_meta_post' )
			) );

//            register_rest_route( 'pda-ip-block/v1', '/get-user-roles-settings', array(
//                'methods' => 'GET',
//                'callback' => array($this, 'get_user_roles_in_settings')
//            ));

			//api for membership integration
			register_rest_route( 'pda-memberships-integration/v1', '/get-members-select/(?P<post_id>[0-9-]+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'get_members_in_meta_post' )
			) );

			register_rest_route( 'pda/v1', 'pdf/(?P<id>[0-9]+)', array(
				'methods' => 'POST',
				'callback' => array( $this, 'get_pdf_content' )
			));

			register_rest_route( 'pda/v1', '/debug/(?<id>[0-9]+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'debug' )
			) );

            register_rest_route( 'pda-magic-link/v1', '/add-private-link-for-magic-link/(?P<post_id>[0-9-]+)', array(
                'methods'  => 'POST',
                'callback' => array( $this, 'add_private_link_for_magic_link' )
            ) );

            register_rest_route( 'pda-magic-link/v1', '/get-private-link-for-magic-link/(?P<post_id>[0-9-]+)', array(
                'methods'  => 'GET',
                'callback' => array( $this, 'get_private_link_for_magic_link' )
            ) );
		}

		function get_private_link_for_magic_link($data) {
            $service = new PDA_Services();
            return $service->handle_data_for_get_private_link_magic_link($data);
        }

		function add_private_link_for_magic_link($data) {
            $service = new PDA_Services();
            $service->check_before_create_private_link_for_magic_link($data);
        }

		public function debug( $data ) {
//			global $wpdb;
//
//			$table_name = $wpdb->prefix . 'prevent_direct_access';
//
//			$columns = [];
//			foreach ( $wpdb->get_col( "DESC " . $table_name, 0 ) as $column_name ) {
//
//				array_push( $columns, $column_name );
//
//			}
//
//			return array(
//				'db_version' => get_option( PDA_v3_Constants::$db_version ),
//				'columns'    => $columns
//			);
//			$service = new PDA_Services();
//			$info = $service->handle_license_info();
//			$cronjob_handler = new PDA_Cronjob_Handler();
//			$cronjob_handler->schedule_ls_cron_job();
//
//			return version_compare( PDA_GOLD_V3_VERSION, '3.0.19.5', '<=' );
			$our_plugin = plugin_basename( __FILE__ );
			return $our_plugin;
		}

		/**
		 * Get user roles in settings
		 * @return array
		 */
		public function get_user_roles_in_settings( $data ) {
			$settings = get_option( PDA_v3_Constants::OPTION_NAME );
			$results  = array(
				"file_access_permission" => "",
				"whitelist_roles"        => "",
			);
			if ( $settings ) {
				$options = unserialize( $settings );
				if ( array_key_exists( PDA_v3_Constants::FILE_ACCESS_PERMISSION, $options ) ) {
					$whitelist = "";
					if ( array_key_exists( PDA_v3_Constants::WHITElIST_ROLES, $options ) && $options[PDA_v3_Constants::WHITElIST_ROLES] != "" ) {
						$whitelist = implode( " - ", $options[PDA_v3_Constants::WHITElIST_ROLES] );
					}
					$results = [
						"file_access_permission" => $options[PDA_v3_Constants::FILE_ACCESS_PERMISSION],
						"whitelist_roles"        => $whitelist,
					];
				}
			}

			return $results;
		}

		function get_members_in_meta_post( $data ) {
			$data = get_post_meta( $data["post_id"], PDA_v3_Constants::$pda_meta_key_memberships_integration );

			return $data;
		}

		function get_user_roles_in_meta_post( $data ) {
			$data = get_post_meta( $data["post_id"], PDA_v3_Constants::$pda_meta_key_user_roles );
			// TODO: sonmv please refactor it :-?
			$arr  = [
				'type'  => "",
				'roles' => ""
			];
			if ( ! empty( $data ) ) {
				$arr = [
					'type'  => $data[0],
					'roles' => ""
				];
			}

			return $arr;
		}

		function add_user_roles_to_meta_post( $data ) {
			$type = $data["file_access_permision"];
			if ( ! empty( $type ) ) {
				update_post_meta( $data["post_id"], PDA_v3_Constants::$pda_meta_key_user_roles, $type );
			}
			if ( Yme_Plugin_Utils::is_plugin_activated( 'membership' ) === - 1) {
				if ( isset( $data["memberships_fap"] ) && ! empty( $data["memberships_fap"] ) ) {
					if ( $data["memberships_fap"] === 'memberships-default' || $data["memberships_fap"] === 'no-memberships' ) {
						update_post_meta( $data["post_id"], PDA_v3_Constants::$pda_meta_key_memberships_integration, $data["memberships_fap"] );
					} else {
						$data_member_keys = array(
							"user_access_manager",
							"memberships_2",
							"paid_memberships_pro",
							"woo_memberships",
							"woo_subscriptions",
							"ar_member"
						);
						$data_members = array();
						foreach ( $data_member_keys as $key ) {
							if ( isset( $data[ $key ] ) ) {
								$data_members[ $key ] = $data[ $key ];
							}
						}
						if ( ! empty( $data_members ) ) {
							update_post_meta( $data["post_id"], PDA_v3_Constants::$pda_meta_key_memberships_integration, $data_members );
						}
					}
				}
			}
		}

		function generateExpired( $data ) {
			$repo = new PDA_v3_Gold_Repository();

			return $repo->generateExpired( $data );
		}

		function protect_files( $data ) {
			$result = PDA_Private_Link_Services::protect_file( $data['id'] );
			if ( is_wp_error( $result ) ) {
				return $result;
			}
		}

		function list_private_links( $data ) {
			$links = $this->repo->get_private_links_by_post_id_and_type_is_null( $data['id'] );

			return array_map( function ( $link ) {
				$link['full_url'] = Pda_v3_Gold_Helper::get_private_url( $link['url'] );
				$link['time'] = strtotime( $link['time'] );
				return $link;
			}, $links );
		}

		function create_private_urls( $data ) {
			$service = new PDA_Services();
			return $service->check_before_create_private_link($data);
		}

		function update_private_urls( $data ) {
			return $this->repo->update_private_link( $data['id'], $data['info'] );
		}

		function delete_private_urls( $data ) {
			return $this->repo->delete_private_link( $data['id'] ) > 0;
		}

		function delete_private_link( $data ) {
			return $this->repo->delete_private_link_by_uri( $data ) > 0;
		}

		function is_protected( $data ) {
            $setting = new Pda_Gold_Functions;
            if ( $setting->getSettings( PDA_v3_Constants::REMOTE_LOG ) ) {
                $edit_url = wp_get_attachment_url( $data['id'] ) . '?t=' . uniqid();
            } else {
                $edit_url = wp_get_attachment_url( $data['id'] );
            }
			$func = new Pda_Gold_Functions;
            $is_synced = $func->check_file_synced_s3($data['id']) && Yme_Plugin_Utils::is_plugin_activated( 'pdas3' ) === -1;
            $title = get_the_title( $data['id'] ) === '' ? '(no title)' : get_the_title( $data['id'] );

			return array(
				'is_protected' => $this->repo->is_protected_file( $data['id'] ),
				'post'         => array(
					'title'    => $title,
					'edit_url' => $edit_url,
					's3_link'  => $is_synced,
				),
				'role_setting' => $this->get_user_roles_in_settings( $data ),
			);
		}

		function un_protect_files( $data ) {
			return $this->repo->un_protect_file( $data['id'] );
		}

		function remoteLogHandle( $data ) {
			$message = "Site: " . site_url() . ", License's result: " . $data['license_res'];
			error_log( $message );
			$remote = new PDA_Gold_Logger_V3;
			$remote->remote_log( $message, true );
		}

		function migrateData() {
			$attachment_ids = $this->repo->get_protected_posts();
			$names          = array();
			foreach ( $attachment_ids as $id ) {
				$file = get_post_meta( $id['post_id'], '_wp_attached_file', true );
				if ( ! empty( $file ) ) {
					$info = pathinfo( $file );
					if ( false === stripos( $file, Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir( '/' ) ) ) {
						$reldir = dirname( $file );
						if ( in_array( $reldir, array( '\\', '/', '.' ), true ) ) {
							$reldir = '';
						}
						$protected_dir = path_join( Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir(), $reldir );
						Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected( $id['post_id'], $protected_dir );
						add_post_meta( $id['post_id'], PDA_v3_Constants::PROTECTION_META_DATA, true );
						array_push( $names,
							array(
								'basename' => $info['basename'],
								'id'       => $id['post_id']
							)
						);
					}
				}
			}

			$options = new PDA_v3_Gold_Repository();
			$options->migrate_pda_options();

			update_option( PDA_v3_Constants::MIGRATE_DATA, true, 'no' );
			include( 'wp-admin/includes/file.php' );
			Prevent_Direct_Access_Gold_Htaccess::register_rewrite_rules();

			return $names;
		}

		function set_default( $data ) {
			return $this->repo->set_default_private_link( $data['id'], $data['post_id'] );
		}

		function generateExpiredFromPrivateLink( $data ) {
			$url     = $data['url'];
			$service = new PDA_Services;

			return $service->generate_private_link( $url, '.mp4' );
		}

		public function get_pdf_content( $data ) {
			$file_path = get_post_meta( $data['id'], '_wp_attached_file', true );
			if ( ! empty( $file_path ) ) {
				$upload_dir = wp_upload_dir('');
				$file = rtrim( $upload_dir['basedir'], '/' ) . '/' . $file_path;
				if( file_exists( $file ) ) {
					$handle = fopen( $file, 'r');
					$data = fread( $handle, filesize( $file) );
					$encoded = base64_encode( $data );
					return $encoded;
				}
			}
		}

        function generate_expired_for_video( $data ) {
            $repo = new PDA_v3_Gold_Repository();
            $meta = get_post_meta($data['id']);
            $meta_name = explode( "/", $meta['_wp_attached_file'][0] );
            $meta_name = $meta_name[ count($meta_name) - 1 ];
            return array(
                'name' => $meta_name,
                'meta_value' => unserialize($meta['_wp_attachment_metadata'][0]),
                'link' => $repo->generateExpired( $data )
            );
        }
	}
}
