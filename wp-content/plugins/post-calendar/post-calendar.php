<?php
/*
Plugin Name: PostCalendar
Version: 1.0.0
Description: Output a calendar of posts.
Author: Matt Jennings
Author URI: https://www.mattjennings.net/
Plugin URI: https://www.mattjennings.net/
*/

class PostCalendar {

    private $version = '1.0.0';

    public function __construct() {
        add_shortcode('postcalendar', [$this, 'displayCalendar']);
    }

    public function displayCalendar($atts, $content = null) {

        // Get requested month from URL
        // Validate current month from URL
        // If month and year not valid, put in defaults
        // which will be current month and year
        if(array_key_exists('month', $_GET)) {
            $month = (int)$_GET['month'];

            if($month < 1 || $month > 12) {
                $month = (int)date('n');
            }
        }
        else {
            $month = (int)date('n');
        }


        // Get requested year from URL
        // Validate current year from URL
        // If month and year not valid, put in defaults
        // which will be current month and year
        if(array_key_exists('yr', $_GET)) {
            $year = (int)$_GET['yr'];

            if($year < 1990 || $year > date('Y')) {
                $year = (int)date('Y');
            }
        }
        else {
            $year = (int)date('Y');
        }


        $d = new DateTime($year.'-'.$month.'-01');

        // Need to know the name of the month
        $monthName = $d->format('F');




        // Get all posts related to a month and output
        $posts = get_posts([
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'asc',
            'year' => $year,
            'monthnum' => $month
        ]);


        // Need to know what the month and year for the previous and next buttons
        $daysToSubtract = $d->format('w');
        if($daysToSubtract == 0) {
            $daysToSubtract = 7;
        }

        $prev = clone $d;
        $prev->sub(new DateInterval('P' . $daysToSubtract .  'D'));
        $prevUrl = get_permalink().'?month='.$prev->format('m').'&yr='.$prev->format('Y');

        $next = clone $d;
        $next->add(new DateInterval('P1M'));
        $nextUrl = get_permalink().'?month='.$next->format('m').'&yr='.$next->format('Y');

        // Need to know how many days are in the previous month
        $startDateNumber = $prev->format('j');

        $prev->modify('last day of this month');
        $lastDayOfPreviousMonth = $prev->format('j');

        $d->modify('last day of this month');
        $lastDayOfCurrentMonth = $d->format('j');

        $rows = '<tr>';
        $dayCounter = 0; // Sunday


        // Output the cells for the days from the prior month
        if ($lastDayOfPreviousMonth - $startDateNumber != 6) {
            for ($i = $startDateNumber; $i <= $lastDayOfPreviousMonth; $i++) {

                $rows .= '<td>'.$i . '</td>';
                $dayCounter++;
            }
        }

        // Loop through all the days of the current month
        $currentPost = 0;
        $postCount = sizeof($posts);

        for ($i = 1; $i <= $lastDayOfCurrentMonth; $i++) {
            $rows .= '<td>'.$i;

            while($currentPost < $postCount && date('j', strtotime($posts[$currentPost]->post_date)) == $i) {
                $rows .= '<br /><a href="' . get_permalink($posts[$currentPost]) . '">' . $posts[$currentPost]->post_title . '</a><br />-';
                $currentPost++;
            }

            $rows .= '</td>';
            $dayCounter++;
            if ($dayCounter == 7) {
                $dayCounter = 0;
                $rows .= "</tr><tr>";
            }
        }

        // Fill the last row with dates from next month
        if ($dayCounter > 0) {
            $day = 1;
            for($i = $dayCounter; $i < 7; $i++) {
                $rows .= '<td>'.$day.'</td>';
                $day++;
            }
        }

        // Close last row
        $rows .= '</tr>';


        $output = <<<CALENDAR
        <div class="postcalendar">
            <div class="pager">
                <span class="page-btn prev"><a href="{$prevUrl}">&lt;</a></span>
                <span class="current-month-year">{$monthName} {$year}</span>
                <span class="page-btn next"><a href="{$nextUrl}">&gt;</a></span>                
            </div>
            <table class="calendar">
                <tr>
                    <th>Sun</th>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>                  
                </tr>
                {$rows}                               
            </table>
        </div>
CALENDAR;

        return $output;
    }

}
$postCalendar = new PostCalendar();