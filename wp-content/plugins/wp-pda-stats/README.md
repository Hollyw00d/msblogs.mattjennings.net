# wp-pda-stats

### Change logs

**1.0.5: 25 September, 2018**

- Add new criteria to track views/clicks by users for shared private links

**1.0.4: 13 August, 2018**

- Fix duplicate settings sub menu

**1.0.3: June 6, 2018**

- Integrate with PDA Gold version 3

**1.0.0: March 24, 2018**

- Show top ten private link having the most hit counts

- Show the file having the most private links's hit counts

- Show image hot linking data

