<?php

if ( ! class_exists( 'PDA_Stats_Repository' ) ) {
    class PDA_Stats_Repository {

        private $wpdb;
        private $table_name;

        public function __construct() {
            global $wpdb;
            $this->wpdb       = &$wpdb;
            $this->table_name = $wpdb->prefix . 'prevent_direct_access_statistics';
        }

        function insert_data_to_db( $user_id, $link_id, $link_type, $can_view ) {
            $has_data_db = $this->check_data_isset_db( $user_id, $link_id, $link_type, $can_view );
            if ( empty( $has_data_db ) ) {
                if ( $link_type === PDA_v3_Constants::PDA_ORIGINAL_LINK ) {
                    $post = get_post( $link_id );
                    if ( ! isset( $post ) ) {
                        return;
                    }
                }
                $this->wpdb->insert(
                    $this->table_name,
                    array(
                        'link_id'   => $link_id,
                        'user_id'   => $user_id,
                        'link_type' => $link_type,
                        'count'     => 1,
                        'can_view'  => $can_view
                    )
                );
            } else {
                $this->wpdb->update(
                    $this->table_name,
                    array(
                        'count' => $has_data_db->count + 1,
                    ),
                    array(
                        'ID' => $has_data_db->ID,
                    )
                );
            }
        }

        function check_data_isset_db( $user_id, $link_id, $link_type, $can_view ) {
            $query_string = $this->wpdb->prepare( "SELECT * FROM $this->table_name WHERE link_id = %s AND user_id = %s AND link_type = %s AND can_view = %s", $link_id, $user_id, $link_type, $can_view );
            $result       = $this->wpdb->get_row( $query_string );

            return $result;
        }

        function get_all_data_in_db() {
            $query_string = "SELECT * FROM $this->table_name";
            $results      = $this->wpdb->get_results( $query_string );

            return $results;
        }

        function convert_data_for_api() {
            $tracking_data     = $this->get_all_data_in_db();
            $only_existed_post = array_values( array_filter( $tracking_data, function ( $data ) {
                if ( $data->link_type === PDA_v3_Constants::PDA_ORIGINAL_LINK ) {
                    $post = get_post( $data->link_id );
                    if ( ! is_null( $post ) ) {
                        return $data;
                    }
                } else if ( $data->link_type === PDA_v3_Constants::PDA_PRIVATE_LINK ) {
                    $private_link = $this->get_private_link_by_id( $data->link_id );
                    if ( $private_link !== null ) {
                        $post = get_post( $private_link->post_id );
                        if ( ! is_null( $post ) ) {
                            return $data;
                        }
                    }
                }
            } ) );
            $all_data          = array_map( function ( $data ) {
                $admin = new Wp_Pda_Stats_Admin( "", "" );
                if ( $data->link_type === PDA_v3_Constants::PDA_PRIVATE_LINK ) {
                    list( $edit_link, $file_name, $full_link, $user_name, $user_info ) = $this->massage_private_link( $data, $admin );
                } else {
                    list( $edit_link, $full_link, $file_name, $user_name, $user_info ) = $this->massage_original_link( $data, $admin );
                }

                return [
                    'full_link' => $full_link,
                    'user_name' => $user_name,
                    'user_info' => $user_info,
                    'clicks'    => (int) $data->count,
                    'link_type' => $data->link_type === PDA_v3_Constants::PDA_PRIVATE_LINK ? 'Private Link' : 'Original Link',
                    'file_name' => [
                        'name' => empty( $file_name ) ? 'N/A' : $file_name->post_title,
                        'link' => is_null( $edit_link ) ? 'N/A' : $edit_link,
                    ],
                    'can_view'  => $data->can_view == 1 ? 'Yes' : 'No',
                ];
            }, $only_existed_post );

            return $all_data;
        }

        function get_private_link_by_id( $id ) {
            $pda_table    = $this->wpdb->prefix . 'prevent_direct_access';
            $query_string = $this->wpdb->prepare( " SELECT * FROM $pda_table WHERE ID = %s ", $id );
            $results      = $this->wpdb->get_row( $query_string );

            return $results;
        }

        function get_link_profile_user( $user_id ) {
            $link_profile = add_query_arg( 'user_id', $user_id, self_admin_url( 'user-edit.php' ) );

            return $link_profile;
        }

        /**
         * Massage private link before return to client.
         *
         * @param mixed $data Tracking data.
         * @param mixed $admin Admin helper functions.
         *
         * @return array
         */
        private function massage_private_link( $data, $admin ) {
            $private_link = $this->get_private_link_by_id( $data->link_id );
            $edit_link    = $admin->get_edit_post_link_for_api( $private_link->post_id, '' );
            $file_name    = get_post( $private_link->post_id );
            $helper       = new Pda_v3_Gold_Helper();
            $full_link    = $helper->get_private_url( $private_link->url );
            if ( ! empty( get_userdata( $data->user_id ) ) ) {
                $user_name = get_userdata( $data->user_id )->user_login;
                $user_info = $this->get_link_profile_user( $data->user_id );
            } else {
                $user_name = PDA_Stats_Constants::PDA_ANONYMOUS;
                $user_info = '';
            }

            return array( $edit_link, $file_name, $full_link, $user_name, $user_info );
        }

        /**
         * Massage original link before return to client.
         *
         * @param mixed $data Tracking data.
         * @param mixed $admin Admin helper functions.
         *
         * @return array
         */
        private function massage_original_link( $data, $admin ) {
            $edit_link = $admin->get_edit_post_link_for_api( $data->link_id, '' );
            $full_link = wp_get_attachment_url( $data->link_id );
            $file_name = get_post( $data->link_id );
            if ( ! empty( get_userdata( $data->user_id ) ) ) {
                $user_name = get_userdata( $data->user_id )->user_login;
                $user_info = $this->get_link_profile_user( $data->user_id );
            } else {
                $user_name = PDA_Stats_Constants::PDA_ANONYMOUS;
                $user_info = '';
            }

            return array( $edit_link, $full_link, $file_name, $user_name, $user_info );
        }

    }
}