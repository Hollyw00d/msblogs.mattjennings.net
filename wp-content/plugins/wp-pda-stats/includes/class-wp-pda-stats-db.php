<?php

class Wp_Pda_Stats_Db {

    public static function create_table() {

        global $wpdb;
        $jal_db_version = '1.0';

        $table_name = $wpdb->prefix . 'pda_hotlinking';
        if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name ) {
            //table is not created. you may create the table here.
            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
	    	ID mediumint(9) NOT NULL AUTO_INCREMENT,
	    	post_id mediumint(9) NOT NULL,
	    	domain varchar(100) DEFAULT '' NOT NULL,
	    	UNIQUE KEY id (id)
	    ) $charset_collate;";

            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
            dbDelta( $sql );
            add_option( 'jal_db_version_stats', $jal_db_version );
        }
        Wp_Pda_Stats_Db::add_column_to_table();
        Wp_Pda_Stats_Db::create_table_statistics();
    }

    public static function add_column_to_table() {
        global $wpdb;
        $table = $wpdb->prefix . 'prevent_direct_access';
        if ( ! $wpdb->get_col( "SHOW COLUMNS FROM $table LIKE 'country'" ) ) {
            $wpdb->query( sprintf( "ALTER TABLE %s ADD country VARCHAR(255) DEFAULT '' NULL", $table ) );
        }
        if ( ! $wpdb->get_col( "SHOW COLUMNS FROM $table LIKE 'browser'" ) ) {
            $wpdb->query( sprintf( "ALTER TABLE %s ADD browser VARCHAR(255) DEFAULT '' NULL", $table ) );
        }
    }

    public static function insert_tables( $post_id, $referer ) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'pda_hotlinking';
        $data       = Wp_Pda_Stats_Db::get_post_id_db( $post_id );

        if ( empty( $data ) ) {
            $wpdb->insert(
                $table_name,
                array(
                    'post_id' => $post_id,
                    'domain'  => $referer
                )
            );
        } else {
            $domain_db     = $data[0]->domain;
            $arr_domain_db = explode( ';', $domain_db );
            if ( ! in_array( $referer, $arr_domain_db ) ) {
                array_push( $arr_domain_db, $referer );
            }
            $domain = join( ";", $arr_domain_db );
            $wpdb->update(
                $table_name,
                array(
                    'domain' => $domain
                ),
                array(
                    'post_id' => $post_id
                )
            );
        }

    }

    public static function get_post_id_db( $post_id ) {
        global $wpdb;
        $results = $wpdb->get_results( " SELECT * FROM {$wpdb->prefix}pda_hotlinking WHERE post_id = $post_id " );

        return $results;
    }

    public static function create_table_statistics() {
        global $wpdb;
        $jal_db_version_stats = '1.0';

        $table_name = $wpdb->prefix . 'prevent_direct_access_statistics';
        if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name ) {
            //table is not created. you may create the table here.
            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name (
	    	ID mediumint(9) NOT NULL AUTO_INCREMENT,
	    	link_id mediumint(9) NOT NULL,
	    	user_id mediumint(9) DEFAULT -1 NOT NULL,
	    	link_type varchar(50) DEFAULT '' NOT NULL,
	    	count mediumint(9) NOT NULL,
	    	can_view tinyint(0) DEFAULT 0,
	    	UNIQUE KEY id (id)
	    ) $charset_collate;";

            require_once ABSPATH . 'wp-admin/includes/upgrade.php';
            dbDelta( $sql );
            add_option( 'jal_db_version_pda_stats', $jal_db_version_stats );
        }
    }

    public static function drop_table_and_version() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'prevent_direct_access_statistics';
        $wpdb->query( "DROP TABLE IF EXISTS $table_name" );
        delete_option( 'jal_db_version_pda_stats' );
    }

}