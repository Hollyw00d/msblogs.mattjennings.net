<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.buildwps.com/
 * @since      1.0.0
 *
 * @package    Wp_Pda_Stats
 * @subpackage Wp_Pda_Stats/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Pda_Stats
 * @subpackage Wp_Pda_Stats/includes
 * @author     BWPS <hello@ymese.com>
 */
class Wp_Pda_Stats_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate($plugin_basename) {
        $configs = require('class-wp-pda-configs.php');

        $yme_addon = new YME_Addon('pda-stats');
        $addOnIsValid = $yme_addon->isValidPurchased($configs->addonProductId, Yme_Plugin::getLicenseKey('pda'));
        $addOnIsValidV3 = $yme_addon->isValidPurchased($configs->addonProductId, Yme_Plugin::getLicenseKey('pdav3'));

        if (Yme_Plugin_Utils::is_plugin_activated( 'pda_gold' ) === 1 && Yme_Plugin_Utils::is_plugin_activated( 'pda_v3' ) === 1) {
            deactivate_plugins( plugin_basename( __FILE__ ) );
            wp_die( YME_MESSAGES::$PDA['PDA_NEVER_PURCHASED'] );
        }

        if ( Yme_Plugin_Utils::is_plugin_activated('pda_gold') === 2 && Yme_Plugin_Utils::is_plugin_activated('pda_v3') === 2 ) {
            deactivate_plugins( $plugin_basename );
            wp_die( YME_MESSAGES::$PDA['PDA_UPDATE_VERSION'] );
        }

        if( ! $addOnIsValid['isValid'] && ! $addOnIsValidV3['isValid']) {
            deactivate_plugins( $plugin_basename );
            wp_die( YME_MESSAGES::$PDA['PDA_ADDON_STOLEN'] );
        }
	}

}
