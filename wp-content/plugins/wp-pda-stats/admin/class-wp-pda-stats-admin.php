<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.buildwps.com/
 * @since      1.0.0
 *
 * @package    Wp_Pda_Stats
 * @subpackage Wp_Pda_Stats/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Pda_Stats
 * @subpackage Wp_Pda_Stats/admin
 * @author     BWPS <hello@ymese.com>
 */
class Wp_Pda_Stats_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     *
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wp_Pda_Stats_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wp_Pda_Stats_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        // wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-pda-stats-admin.css', array(), $this->version, 'all' );

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Wp_Pda_Stats_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Wp_Pda_Stats_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        // wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-pda-stats-admin.js', array( 'jquery' ), $this->version, false );

    }

    public function check_plugin_pda_activate() {
        if ( Yme_Plugin_Utils::is_plugin_activated( 'pda_gold' ) === 0 ) {
            ?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( $this->plugin_name ) . _e( ': ' . YME_MESSAGES::$PDA['PDA_NEVER_PURCHASED'], 'pda-stats' ); ?></p>
            </div>
            <?php
        }
        if ( Yme_Plugin_Utils::is_plugin_activated( 'pda_gold' ) === 2 ) {
            ?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e( $this->plugin_name ) . _e( ': ' . YME_MESSAGES::$PDA['PDA_UPDATE_VERSION'], 'pda-stats' ); ?></p>
            </div>
            <?php
        }
    }

    public function add_submenu_pda() {
        $stats_sub_menu    = add_submenu_page( 'wp_pda_gold_options', __( 'Statistics', 'pda-settings-page' ), __( 'Statistics', 'pda-settings-page' ), 'manage_options', 'statistics', array(
            $this,
            'pda_options_do_page_statistic'
        ), 'dashicons-hidden' );
        $stats_sub_menu_v3 = add_submenu_page( 'pda-gold', __( 'Statistics', 'pda-settings-page' ), __( 'Statistics', 'pda-settings-page' ), 'manage_options', 'statistics', array(
            $this,
            'pda_options_do_page_statistic'
        ), 'dashicons-hidden' );
        add_action( 'admin_print_styles-' . $stats_sub_menu, array( $this, 'register_settings_assets' ) );
        add_action( 'admin_print_styles-' . $stats_sub_menu_v3, array( $this, 'register_settings_assets' ) );
    }

    public function pda_options_do_page_statistic() {
        ?>
        <div id="wp-pda-stats-root"></div>
        <?php
    }

    public function register_settings_assets() {
        wp_enqueue_style( $this->plugin_name . 'setttings_stats_style', plugin_dir_url( __FILE__ ) . ( '/js/dist/style/wp-pda-stats.css' ), array() );
        wp_enqueue_script( $this->plugin_name . 'settings_stats', plugin_dir_url( __FILE__ ) . ( '/js/dist/wp-pda-stats-bundle.js' ), array( 'jquery' ) );
        wp_localize_script( $this->plugin_name . 'settings_stats', 'pda_settings_stats', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    }

    public function setup_yme_plugin_rest_api_stats() {
        register_rest_route( '/pda-stats/', 'top-ten-private-links/(?P<limit>[0-9-]+)', array(
            'methods'  => 'GET',
            'callback' => array( $this, 'top_ten_result' )
        ) );
        register_rest_route( '/pda-stats/', 'file-most-private-links-clicks/(?P<limit>[0-9-]+)', array(
            'methods'  => 'GET',
            'callback' => array( $this, 'file_most_private_clicks' )
        ) );
        register_rest_route( '/pda-stats/', 'file-and-domain', array(
            'method'   => 'GET',
            'callback' => array( $this, 'get_file_and_domain' )
        ) );
        register_rest_route( '/pda-stats/', 'top-ten-files/(?P<limit>[0-9-]+)', array(
            'method'   => 'GET',
            'callback' => array( $this, 'file_most_private_links' )
        ) );

        register_rest_route( '/pda-stats/', 'private-summary/(?P<post_id>[0-9-]+)', array(
            'method'   => 'GET',
            'callback' => array( $this, 'private_summary' )
        ) );

        register_rest_route( '/pda-stats/', 'going-to-expired-links/(?P<limit>[0-9-]+)', array(
            'method'   => 'GET',
            'callback' => array( $this, 'going_to_expired_links' )
        ) );

        register_rest_route( '/pda-stats', 'top-ten-country', array(
            'method'   => 'GET',
            'callback' => array( $this, 'top_ten_country' )
        ) );

        register_rest_route( '/pda-stats', 'top-ten-browser', array(
            'method'   => 'GET',
            'callback' => array( $this, 'top_ten_browser' )
        ) );

        register_rest_route( '/pda-stats', 'all-data-in-pda-stats', array(
            'method'   => 'GET',
            'callback' => array( $this, 'all_data_in_pda_stats' )
        ) );

        register_rest_route( '/pda-stats', 'stats-for-share-private-link', array(
            'method'   => 'GET',
            'callback' => array( $this, 'get_data_for_share_private_link' )
        ) );
    }

    function get_data_for_share_private_link() {
        if ( Yme_Plugin_Utils::is_plugin_activated( 'magic_link' ) === - 1 ) {
            global $wpdb;
            $pda_v3_table     = $wpdb->prefix . 'prevent_direct_access';
            $magic_link_table = $wpdb->prefix . 'prevent_direct_access_downloads';

            $query_string = "SELECT $pda_v3_table.post_id, $pda_v3_table.time, $pda_v3_table.url, $pda_v3_table.limit_downloads, $pda_v3_table.expired_date, $pda_v3_table.roles, $magic_link_table.user_id, $magic_link_table.downloads, $magic_link_table.expired_date as created_date FROM $pda_v3_table INNER JOIN $magic_link_table ON $pda_v3_table.ID = $magic_link_table.private_link_id";
            $full_data    = $wpdb->get_results( $query_string );

            $results = array_reduce( $full_data, function ( $pre, $data ) {
                if ( is_null( get_post( $data->post_id ) ) ) {
                    return array();
                }
                $helper    = new Pda_v3_Gold_Helper();
                $full_link = $helper->get_private_url( $data->url );
                $post      = get_post( $data->post_id );
                $edit_link = $this->get_edit_post_link_for_api( $data->post_id, '' );

                $user_info  = get_userdata( $data->user_id );
                $user_name  = $user_info->user_login;
                $user_roles = implode( ";", $user_info->roles );

                $repo      = new PDA_Stats_Repository();
                $link_user = $repo->get_link_profile_user( $data->user_id );
                if ( $data->expired_date === null ) {
                    $expired_date = null;
                } else {
                    $expire_day   = date( 'Y-m-d', $data->expired_date );
                    $create_day   = date_format( date_create( $data->time ), 'Y-m-d' );
                    $date1        = new DateTime( $expire_day );
                    $date2        = new DateTime( $create_day );
                    $result       = $date1->diff( $date2 );
                    $days         = $result->days;
                    $expired_date = strtotime( '+' . $days . ' days', $data->created_date );
                }

                $massage_data = array(
                    'roles_access'    => str_replace( ";", ", ", $data->roles ),
                    'user'            => array(
                        'user_name'  => $user_name,
                        'link_user'  => $link_user,
                        'user_roles' => $user_roles,
                    ),
                    'url'             => $data->url,
                    'full_url'        => $full_link,
                    'file_name'       => array(
                        'name' => $post->post_title,
                        'link' => $edit_link,
                    ),
                    'created_time'    => strtotime( $data->time ),
                    'expired_date'    => $expired_date,
                    'downloads_limit' => $data->limit_downloads,
                    'downloads'       => (int) $data->downloads,
                    'first_download'  => $data->created_date,
                );

                return array_merge( $pre, array( $massage_data ) );
            }, array() );

            return $results;
        }
    }

    public function all_data_in_pda_stats() {
        $repo     = new PDA_Stats_Repository();
        $all_data = $repo->convert_data_for_api();

        return $all_data;
    }

    public function top_ten_browser() {
        global $wpdb;
        $prevent_table = $wpdb->prefix . 'prevent_direct_access';
        $query         = "SELECT browser FROM {$prevent_table} WHERE browser != ''";
        $query_result  = $wpdb->get_results( $query );

        $query_result = array_map( function ( $data ) {
            $try = unserialize( $data->browser );
            if ( ! $try ) {
                return null;
            } else {
                return unserialize( $data->browser );
            }
        }, $query_result );

        $query_result = array_filter( $query_result, function ( $d ) {
            return $d != null;
        } );

        $summary = [];
        foreach ( $query_result as $key => $value ) {
            $countries = array_keys( $value );
            foreach ( $countries as $c ) {
                if ( ! array_key_exists( $c, $summary ) ) {
                    $summary[ $c ] = $value[ $c ];
                } else {
                    $summary[ $c ] += $value[ $c ];
                }
            }
        }
        arsort( $summary );

        return $summary;
    }

    public function top_ten_country() {
        global $wpdb;
        $prevent_table = $wpdb->prefix . 'prevent_direct_access';
        $query         = "SELECT country FROM {$prevent_table} WHERE country != '' ";
        $query_result  = $wpdb->get_results( $query );

        $query_result = array_map( function ( $data ) {
            return unserialize( $data->country );
        }, $query_result );

        $summary = [];
        foreach ( $query_result as $key => $value ) {
            $countries = array_keys( $value );
            foreach ( $countries as $c ) {
                if ( ! array_key_exists( $c, $summary ) ) {
                    $summary[ $c ] = $value[ $c ];
                } else {
                    $summary[ $c ] += $value[ $c ];
                }
            }
        }
        arsort( $summary );

        return $summary;
    }

    public function private_summary( $data ) {
        global $wpdb;
        $prevent_table              = $wpdb->prefix . 'prevent_direct_access';
        $query_total_private_link   = "SELECT COUNT(url) FROM {$prevent_table} WHERE post_id = {$data["post_id"]}";
        $query_total_click          = "SELECT SUM(hits_count) FROM {$prevent_table} WHERE post_id = {$data["post_id"]}";
        $results_total_private_link = $wpdb->get_var( $query_total_private_link );
        $results_total_click        = $wpdb->get_var( $query_total_click );
        if ( $results_total_click == null ) {
            $results_total_click = "0";
        }

        return [
            "total_private_link" => $results_total_private_link,
            "total_click"        => $results_total_click,
        ];
    }

    public function get_file_and_domain() {
        global $wpdb;
        $pda_hotlinkling_table = $wpdb->prefix . 'pda_hotlinking';
        $posts_table           = $wpdb->prefix . 'posts';
        $query                 = "SELECT {$posts_table}.ID as id, {$posts_table}.guid as filename, {$pda_hotlinkling_table}.domain FROM {$pda_hotlinkling_table} JOIN {$posts_table} ON {$pda_hotlinkling_table}.post_id = {$posts_table}.ID";
        $results               = $wpdb->get_results( $query );
        $results               = array_map( array( $this, 'cut_file_name_by_url' ), $results );

        return $results;
    }

    public function cut_file_name_by_url( $result ) {
        $result->edit_link = $this->get_edit_post_link_for_api( $result->id, '' );
        $filename          = explode( '/', $result->filename );
        $result->filename  = end( $filename );

        return $result;
    }

    public function top_ten_result( $data ) {
        global $wpdb;
        $prevent_table = $wpdb->prefix . 'prevent_direct_access';
        $posts_table   = $wpdb->prefix . 'posts';
        $query         = "SELECT {$posts_table}.ID as id, {$posts_table}.guid as filename, {$prevent_table}.url, {$prevent_table}.hits_count FROM {$prevent_table} INNER JOIN {$posts_table} ON {$prevent_table}.post_id = {$posts_table}.ID ORDER BY {$prevent_table}.hits_count DESC LIMIT %d";
        $query_result  = $wpdb->get_results( $wpdb->prepare( $query, $data['limit'] ) );
        $query_result  = array_map( array( $this, 'map_value_top_ten' ), $query_result );
        $query_result  = array_filter( $query_result, function ( $q ) {
            return $q->hits_count != 0;
        } );

        return $query_result;
    }

    public function get_edit_post_link_for_api( $id, $context = 'display' ) {
        if ( ! $post = get_post( $id ) ) {
            return;
        }

        if ( 'revision' === $post->post_type ) {
            $action = '';
        } elseif ( 'display' == $context ) {
            $action = '&amp;action=edit';
        } else {
            $action = '&action=edit';
        }

        $post_type_object = get_post_type_object( $post->post_type );
        if ( ! $post_type_object ) {
            return;
        }

        if ( $post_type_object->_edit_link ) {
            $link = admin_url( sprintf( $post_type_object->_edit_link . $action, $post->ID ) );
        } else {
            $link = '';
        }

        return $link;
    }

    private function get_full_private_url( $url ) {
        $func       = new Pda_Gold_Functions();
        $prefix_url = $func->prefix_roles_name( PDA_v3_Constants::PDA_PREFIX_URL );
        if ( isset( $prefix_url ) && ! empty( $prefix_url ) ) {
            if ( ! is_multisite() ) {
                $url = site_url() . "/{$prefix_url}/" . $url;
            } else {
                $url = site_url() . "/{$prefix_url}/site/" . get_current_blog_id() . '/' . $url;
            }
        }

        return $url;
    }

    public function map_value_top_ten( $v ) {
        $v->edit_link = $this->get_edit_post_link_for_api( $v->id, '' );
        $filename     = explode( '/', $v->filename );
        $v->filename  = end( $filename );
        if ( property_exists( $v, 'url' ) ) {
            $v->full_url = $this->get_full_private_url( $v->url );
        }

        return $v;
    }

    public function file_most_private_clicks( $data ) {
        global $wpdb;
        $prevent_table = $wpdb->prefix . 'prevent_direct_access';
        $posts_table   = $wpdb->prefix . 'posts';
        $query         = "SELECT {$posts_table}.ID as id, {$posts_table}.guid as filename, SUM(hits_count) as sum_click FROM {$prevent_table} INNER JOIN {$posts_table} ON {$prevent_table}.post_id = {$posts_table}.ID group by post_id ORDER BY sum_click DESC LIMIT %d";
        $query_result  = $wpdb->get_results( $wpdb->prepare( $query, $data['limit'] ) );
        $query_result  = array_map( array( $this, 'map_value_top_ten' ), $query_result );
        $query_result  = array_filter( $query_result, function ( $q ) {
            return $q->sum_click != 0;
        } );

        return $query_result;

    }

    public function file_most_private_links( $data ) {
        global $wpdb;
        $prevent_table = $wpdb->prefix . 'prevent_direct_access';
        $posts_table   = $wpdb->prefix . 'posts';
        $query         = "SELECT {$posts_table}.ID as id, {$posts_table}.guid as filename, SUM(is_prevented) as sum_private_link FROM {$prevent_table} INNER JOIN {$posts_table} ON {$prevent_table}.post_id = {$posts_table}.ID group by post_id ORDER BY sum_private_link DESC LIMIT %d";
        $query_result  = $wpdb->get_results( $wpdb->prepare( $query, $data['limit'] ) );
        $query_result  = array_map( array( $this, 'map_value_top_ten' ), $query_result );

        return $query_result;
    }

    public function get_post_id( $data ) {
        $post_id = $data['post_id'];
        $domain  = $data['domain'];
        Wp_Pda_Stats_Db::insert_tables( $post_id, $domain );
    }

    public function going_to_expired_links( $data ) {
        global $wpdb;
        $prevent_table = $wpdb->prefix . 'prevent_direct_access';
        $posts_table   = $wpdb->prefix . 'posts';
        $queryString   =
            "SELECT {$posts_table}.ID as id, {$posts_table}.guid as filename, url, expired_date  FROM $prevent_table INNER JOIN $posts_table ON {$prevent_table}.post_id = {$posts_table}.ID " .
            "WHERE expired_date > UNIX_TIMESTAMP(CURRENT_TIMESTAMP) AND is_prevented = \"1\" " .
            "order by expired_date asc limit %d";
        $preparation   = $wpdb->prepare( $queryString, $data['limit'] );
        $query_result  = $wpdb->get_results( $preparation );

        $results = array_map( array( $this, 'map_value_top_ten' ), $query_result );

        return $results;
    }

    public function map_full_url( $link ) {
        if ( property_exists( $link, 'url' ) ) {
            $link->full_url = $this->get_full_private_url( $link->url );
        }

        return $link;
    }

    public function get_country( $id ) {
        $ip = $_SERVER['REMOTE_ADDR'];
//        $ip = "116.110.101.159"; //VietNam
//        $ip = "100.56.78.99"; //United States
//        $ip = "116.11.101.159"; //China
        $details = json_decode( file_get_contents( "http://api.ipstack.com/${ip}?access_key=9c6d2647b407a98be7ee75db653d0137" ) );
        $data    = $this->get_info_file_by_id( $id );
        empty( $data->country ) ? $countries = [] : $countries = unserialize( $data->country );

        if ( isset( $countries[ $details->country_name ] ) ) {
            $countries[ $details->country_name ] = $countries[ $details->country_name ] + 1;
        } else {
            $countries[ $details->country_name ] = 1;
        }

        return $countries;
    }

    public function get_browser( $id ) {
        $browser_info         = $this->getBrowserFromUA();
        $browser_name_version = $browser_info['name'] . "-" . $browser_info['version'];
        $data                 = $this->get_info_file_by_id( $id );

        empty( $data->browser ) ? $browsers = [] : $browsers = unserialize( $data->browser );

        if ( isset( $browsers[ $browser_name_version ] ) ) {
            $browsers[ $browser_name_version ] = $browsers[ $browser_name_version ] + 1;
        } else {
            $browsers[ $browser_name_version ] = 1;
        }

        return $browsers;
//        $arr_browser = explode(";", $data->browser);
//        if ($data->browser != null) {
//            if (!in_array($browser_name_version, $arr_browser)) {
//                array_push($arr_browser, $browser_name_version);
//            }
//            $browser = join(";", $arr_browser);
//        } else {
//            $browser = $browser_name_version;
//        }
//        return $browser;
    }

    public function insert_country_name( $id ) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'prevent_direct_access';
        $browser    = $this->get_browser( $id );
        $country    = $this->get_country( $id );

        $wpdb->update(
            $table_name,
            array(
                'browser' => serialize( $browser ),
                'country' => serialize( $country ),
            ),
            array(
                'ID' => $id
            )
        );
    }

    public function get_info_file_by_id( $id ) {
        global $wpdb;
        $table        = $wpdb->prefix . 'prevent_direct_access';
        $query        = "SELECT * FROM {$table} WHERE ID = {$id}";
        $query_result = $wpdb->get_row( $query );

        return $query_result;
    }

    function getBrowserFromUA() {
        $u_agent  = $_SERVER['HTTP_USER_AGENT'];
        $bname    = 'Unknown';
        $platform = 'Unknown';
        $version  = "";
        // First get the platform?
        if ( preg_match( '/linux/i', $u_agent ) ) {
            $platform = 'linux';
        } elseif ( preg_match( '/macintosh|mac os x/i', $u_agent ) ) {
            $platform = 'mac';
        } elseif ( preg_match( '/windows|win32/i', $u_agent ) ) {
            $platform = 'windows';
        }
        // Next get the name of the useragent yes seperately and for good reason
        if ( preg_match( '/MSIE/i', $u_agent ) && ! preg_match( '/Opera/i', $u_agent ) ) {
            $bname = 'Internet Explorer';
            $ub    = "MSIE";
        } elseif ( preg_match( '/Firefox/i', $u_agent ) ) {
            $bname = 'Mozilla Firefox';
            $ub    = "Firefox";
        } elseif ( preg_match( '/Chrome/i', $u_agent ) ) {
            $bname = 'Google Chrome';
            $ub    = "Chrome";
        } elseif ( preg_match( '/Safari/i', $u_agent ) ) {
            $bname = 'Apple Safari';
            $ub    = "Safari";
        } elseif ( preg_match( '/Opera/i', $u_agent ) ) {
            $bname = 'Opera';
            $ub    = "Opera";
        } elseif ( preg_match( '/Netscape/i', $u_agent ) ) {
            $bname = 'Netscape';
            $ub    = "Netscape";
        }
        // finally get the correct version number
        $known   = array( 'Version', $ub, 'other' );
        $pattern = '#(?<browser>' . join( '|', $known ) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if ( ! preg_match_all( $pattern, $u_agent, $matches ) ) {
            // we have no matching number just continue
        }
        // see how many we have
        $i = count( $matches['browser'] );
        if ( $i != 1 ) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if ( strripos( $u_agent, "Version" ) < strripos( $u_agent, $ub ) ) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }
        // check if we have a number
        if ( $version == null || $version == "" ) {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'   => $pattern
        );
    }

    function delete_table_site_website( $blog_id, $drop ) {
        global $wpdb;
        $table_name = $wpdb->prefix . "pda_hotlinking";
        $sql        = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query( $sql );
    }

    function insert_data_for_pda_stats( $user_id, $link_id, $link_type, $can_view ) {
        $repo = new PDA_Stats_Repository();
        $repo->insert_data_to_db( $user_id, $link_id, $link_type, $can_view );
    }

}


