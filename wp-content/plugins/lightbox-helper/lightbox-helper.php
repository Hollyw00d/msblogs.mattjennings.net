<?php
/*
Plugin Name: Lightbox Helper
Plugin URI: https://www.mattjennings.net/
Description: Lightbox Helper wraps all images (that reside inside a single post) in a <a href="linktoimage.png" data-lightbox></a> tag to enable all images open up in a lightbox wrapper.
Version: 1.0
Author: Matt Jennings
Author URI: https://www.mattjennings.net/
License: GPL2
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: lightbox-helper
*/

class Lightbox_Helper_MJ {
    private static $instance;

    public static function getInstance() {
        if(self::$instance == NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        // implement hooks here

        // Apply filter to single posts
        add_filter('the_content', array($this, 'wrap_images_in_linkbox_anchor_tags'));
    }

    function wrap_images_in_linkbox_anchor_tags($content) {

        // Inside $content (post content) replace '<img src="myimg.png" />' tags with
        // '<a href="myimg.png"><img src="myimg.png" /></a>' tags
        // so that Lightbox can be used to view a larger size of the image
        // Return the $updated_content post ONLY if on
        // a single post
        if(is_single()):
            $updated_content = preg_replace('/<img .*? src="(.*?)" .*?>/im', '<a href="$1"> $0 $2 </a>', $content);
            $content = $updated_content;
        endif;

        return $content;
    }
}

Lightbox_Helper_MJ::getInstance();