=== Click to Tweet Quote using Twitter ===
Contributors: Matt Jennings
Tags: twitter, tweet, click to tweet
Tested up to: 4.9.6
License: GPLv2 or later

Click to Tweet Quote allows you to easily add quotes to your website and then click to Tweet these quotes to post them to Twitter.

== Description ==

Click to Tweet Quote allows you to easily add quotes to your website and then click to Tweet these quotes to post them to Twitter.

== Installation ==

Upload the Click to Tweet Quote plugin to your blog and Activate it.

== Changelog ==
= 1.0.0 =
*Release Date - 2018-05-20*

*
