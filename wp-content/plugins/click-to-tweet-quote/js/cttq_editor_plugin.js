// TinyMCE JS
(function() {
    tinymce.create('tinymce.plugins.cttq_button', {

        // url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            // In url replace '/js' with '/images' to get path to images
            var getImagesUrl = url.replace('/js', '/images');

            //add new button
            ed.addButton('cttq', {
                title : 'Click to Tweet Quote',
                cmd : 'cttq_command',
                image : getImagesUrl + '/twitter-tinymce-icon.png'
            });

            /*
            button functionality to wrap selected text with
            shortcode:
            [tweetquote hashtags=""]Do or not do. There is no try.[/tweetquote]
            */
            ed.addCommand('cttq_command', function() {
                var selected_text = ed.selection.getContent();
                var return_text = '[tweetquote hashtags=""]' + selected_text + '[/tweetquote]';
                ed.execCommand('mceInsertContent', 0, return_text);
            });

        },
        // Button author info
        getInfo : function() {
            return {
                longname : 'Click to Tweet Quote',
                author : 'Matt Jennings',
                version : '1'
            };
        }
    });

    tinymce.PluginManager.add('cttq_button', tinymce.plugins.cttq_button);
})();