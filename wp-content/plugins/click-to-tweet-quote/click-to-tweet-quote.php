<?php
/*
Plugin Name: Click to Tweet Quote using Twitter
Plugin URI: https://www.mattjennings.net/
Description: Click to Tweet Quote allows you to easily add quotes to your website and then click to Tweet these quotes to post them to Twitter.
Version: 1.0
Author: Matt Jennings
Author URI: : https://www.mattjennings.net/
License: GPL2
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: click-to-tweet-quote
*/


/* !0. Table of Contents */

/*
    1. Hooks
        1.1 - Register dashicons in the front end
        1.2 - Registers all custom shortcodes on init
        1.3 - Registers admin menu
        1.4 - Registers plugin settings
        1.5 - Hook to call TinyMCE filters

    2. Shortcodes
        2.1 - cttq_blockquote_register_shortcodes()
        2.2 - cttq_blockquote_shortcode()

    3. Actions
        3.1 - load_dashicons_front_end()

    4. Filters
        4.1 - cttq_admin_menu()

    5. Admin Page
        5.1 - cttq_plugin_settings()
        5.2 - cttq_dashboard_admin_page()

    6. TinyMCE
        6.1 - Check if user has editor permissions and then call TinyMCE filters
        6.2 - Enqueue TinyMCE JS
        6.3 - Register TinyMCE button

*/


/* !1. Hooks */
// 1.1
// Register dashicons in the front end
add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );

// 1.2
// Registers all custom shortcodes on init
add_action('init', 'cttq_blockquote_register_shortcodes');

// 1.3
// Registers admin menu
add_action('admin_menu', 'cttq_admin_menu');

// 1.4
// Registers plugin settings
add_action('admin_menu', 'cttq_plugin_settings');

// 1.5
// Hook to call TinyMCE filters
add_action('init', 'custom_mce_button');


/* !2. Shortcodes */
// 2.1
// Registers custom shortcodes
function cttq_blockquote_register_shortcodes() {
    add_shortcode('tweetquote', 'cttq_blockquote_shortcode');
}

// 2.2
// https://developer.wordpress.org/plugins/shortcodes/shortcodes-with-parameters/
function cttq_blockquote_shortcode( $attributes, $content = null ) {

    // Save ech attribute's values to its own variable.
    // Below are attribute values for hashtags and CSS styles
    extract( shortcode_atts( array(
        'hashtags'          => '',
        'nostyle'           => esc_attr( get_option('cttq_style_option') )
    ),  $attributes ) );

    // Get full URL
    global $wp;
    $current_url = home_url(add_query_arg(array(),$wp->request));

    // setup output variable
    $output = '<a href="#" ' . (($nostyle === "No Style") ? '' : 'style="text-decoration: none;"') . ' onclick="window.open(\'https://twitter.com/intent/tweet?text=' . str_replace(' ', '%20',$content) . '%20' . str_replace(' ', '%20', str_replace('#', '%23', $hashtags)) . '%20' . str_replace('/', '%2F', str_replace(':', '%3A', $current_url)) . '\', \'_blank\', \'width=500,height=500\'); return false;">';
        $output .= '<blockquote' . (($nostyle === "No Style") ? '' :  ' style="display: table; width: 100%;"') .'>';
            $output .= '<div' . (($nostyle === "No Style") ? '' :  ' style="display: table-cell"') . '>';
                $output .= '<span class="dashicons dashicons-format-quote"' . (($nostyle === "No Style" ) ? '' : 'style="color: #1da1f2; font-size: 45px; padding-right: 65px; display: block; float: left; height: 50px;"') . '></span>';
            $output .= '</div>';
            $output .= '<div>';
                // If $content exists assign $output variable
                if(strlen($content)):
                    $output .= '<p ' . (($nostyle === "No Style") ? '' : 'style="display: inline; padding-top: 10px; font-size: 25px; font-style: normal; color: #000;" onmouseover="this.style.color=\'#1da1f2\'" onmouseout="this.style.color=\'#000\'"') . '>' . $content . '</p>';
                endif;
                $output .= '<p ' . (($nostyle === "No Style") ? '' : 'style="color: #1da1f2; font-style: normal;"') . '><i class="dashicons dashicons-twitter"' . (($nostyle === "No Style") ? '' : 'style="color: #1da1f2; font-size: 25px; display: block; float: left; padding-right: 30px;"' ) .'></i>Click to Tweet</p>';
            $output .= '</div>';
        $output .= '</blockquote>';
    $output .= '</a>';

    // return our results/html
    return $output;
}


/* !3. Actions */
// 3.1
// Enable dashicons if the front end
function load_dashicons_front_end() {
    wp_enqueue_style( 'dashicons' );
}


/* !4. Filters */
// 4.1
// register custom plugin admin menu
function cttq_admin_menu() {
    /* main menu */
    $top_menu_item = 'cttq_dashboard_admin_page';

    add_menu_page('', 'Click to Tweet Quote', 'manage_options', $top_menu_item, $top_menu_item, 'dashicons-twitter');
}


/* !5. Admin Page */
// 5.1
// Plugin settings
function cttq_plugin_settings() {
    // Register our settings
    register_setting('cttq-plugin-settings-group', 'cttq_style_option');
    register_setting('cttq-plugin-settings-group', 'cttq_tinymce_option');
}

// 5.2
// dashboard admin page form
function cttq_dashboard_admin_page() {
?>
        <div class="wrap">
            <h2><?php echo __('Click to Tweet Quote for Twitter Options Page'); ?></h2>
            <p><?php echo __('Click to Tweet Quote allows you to easily add quotes to your website and then click to Tweet these quotes to post them to Twitter.'); ?></p>
            <p><?php echo __('<strong>How to use:</strong>'); ?></p>
            <ol>
                <li><?php echo __('Add a new post in WordPress.'); ?></li>
                <li><?php echo __('Add code like the shortcode like the example below, including one or more hashtags like <strong>#yoda</strong>:<br /><code>[tweetquote hashtags="#yoda"]Do or not do. There is no try.[/tweetquote]</code>'); ?></li>
                <li><?php echo __('Save the post to have the shortcode appear with a "Click to Tweet" message under it'); ?></li>
                <li><?php echo __('Choose "Default Style" to apply default styles or "No Style" to apply no styles to the "Click to Tweet" quote as shown below.'); ?></li>
            </ol>
            <?php settings_errors(); ?>

            <form method="post" action="options.php">
                <?php
                settings_fields('cttq-plugin-settings-group');
                do_settings_sections('cttq-plugin-settings-group');
                ?>
                <table class="form-table">    
                    <tr>
                        <th scope="row">
                            <p><label for="cttq_style_option"><?php echo __('Choose Default CSS Style or<br />No Style'); ?></label></p>
                        </th>
                        <td>
                            <p>
                                <select name="cttq_style_option" id="cttq_style_option">
                                    <option <?php echo(empty(esc_attr( get_option('cttq_style_option')))) ? ' selected="selected"' : ''; ?> value="Default Style">Default Style</option>
                                    <option <?php echo(esc_attr( get_option('cttq_style_option') === 'No Style')) ? ' selected="selected"' : ''; ?> value="No Style">No Style</option>
                                </select>
                            </p>
                        </td>
                    </tr>
                </table>
                <p class="submit">
                    <?php submit_button(); ?>
                </p>
            </form>

        </div>
<?php
}


/* !6. TinyMCE */

// 6.1
// Check if user has editor permissions and then call TinyMCE filters
function custom_mce_button() {
    // Check if user have permission
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ):
        return;
    endif;
    if ( 'true' == get_user_option( 'rich_editing' ) ):
        add_filter('mce_external_plugins', 'enqueue_plugin_scripts');
        add_filter('mce_buttons', 'register_buttons_editor');
    endif;
}

// 6.2
// Enqueue TinyMCE JS
function enqueue_plugin_scripts($plugin_array) {
    $plugin_array['cttq_button'] =  plugin_dir_url(__FILE__) . 'js/cttq_editor_plugin.js';
    return $plugin_array;
}


// 6.3
// Register TinyMCE button
function register_buttons_editor($buttons) {
    array_push($buttons, 'cttq');
    return $buttons;
}