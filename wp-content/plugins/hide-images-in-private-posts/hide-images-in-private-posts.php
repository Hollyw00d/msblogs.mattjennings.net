<?php
/*
Plugin Name: Hide Images in Private Posts
Plugin URI: https://www.mattjennings.net/
Description: If a post is post if private (a draft), hide images attached to this draft post (make them 404). If an image is in a draft post and public post, then display it.
Version: 1.0
Author: Matt Jennings
Author URI: https://www.mattjennings.net/
License: GPL2
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: hide-images-from-private-posts
*/

class Hide_Images_In_Private_Posts_MJ {
	private static $instance;

	public static function getInstance() {
		if(self::$instance == NULL) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	// Implement hooks here
	private function __construct() {
		add_action('pre_get_posts', array($this, 'hide_images_in_private_posts'));
	}

	public function hide_images_in_private_posts() {
		global $wp_query;
		$parent_id = intval($wp_query->query_vars['p']);

		if(is_user_logged_in()):

			wp_die($parent_id);

		endif;
	}
}

Hide_Images_In_Private_Posts_MJ::getInstance();