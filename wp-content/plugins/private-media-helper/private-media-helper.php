<?php
/*
Plugin Name: Private Media Helper
Plugin URI: https://www.mattjennings.net/
Description: If a post is post if private (a draft), hide images attached to this draft post (make them 404). If an image is in a draft post and public post, then display it.
Author: Matt Jennings
Version: 1.0
Author URI: https://www.mattjennings.net/
Text Domain: private-media
Domain Path: /private-media/
Support URI: https://www.mattjennings.net/
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Private_Media_Helper_MJ {
	private static $instance;

	public static function getInstance() {
		if(self::$instance == NULL) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct() {
		// Insert text in "Publish" metabox on post or page
		add_action( 'post_submitbox_misc_actions', array( $this, 'show_private_media_in_post' ));

		// Make private images public on post or publish
		add_action('publish_post', array( $this, 'make_private_attached_images_public_on_publish_post' ));
	}

	// Insert text in "Publish" metabox on post or page
	function show_private_media_in_post() {
		global $post;

		if (get_post_type($post) === 'post' || get_post_type($post) === 'page') {
			echo '<div class="misc-pub-section misc-pub-section-last" style="border-top: 1px solid #eee; border-bottom: 1px solid #eee;">';

			$the_content = $post->post_content;

			if(stripos(json_encode($the_content), 'pvtmed')) {
				$private_images_in_post = 'True';
			}
			else {
				$private_images_in_post = 'False';
			}

			echo '<p>Private images in post content?<br />' . $private_images_in_post . '</p>';
			echo '</div>';
		}
	}

	// Make private images public on post or publish
	public function make_private_attached_images_public_on_publish_post($post_id) {
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin' . '/includes/file.php' );
		require_once( ABSPATH . 'wp-admin' . '/includes/media.php' );

		$attached_media = get_attached_media('image', $post_id);
		foreach($attached_media as $attachment) {
			$attachment_id = (int) $attachment->ID;

			$file_data = wp_prepare_attachment_for_js( $attachment_id );

			$file_with_date = str_replace('pvtmed-uploads', '', strstr($file_data['url'], 'pvtmed-uploads'));

			$uploads = wp_upload_dir();

			$new_save_path = $uploads['basedir'] . $file_with_date;
			$old_save_path = str_replace('uploads', 'pvtmed-uploads', $new_save_path);

			if(!file_exists ($new_save_path)) {
				copy($old_save_path, $new_save_path);

				$file_type = wp_check_filetype($new_save_path);

				$new_attachment = array(
					'guid'              => $uploads['url'] . '/' . $file_with_date,
					'post_mime_type'    => $file_type['type'],
					'post_status'       => 'inherit',
					'post_parent'       => $post_id,
					'post_title'        => ''
				);

				$new_attachment_id = wp_insert_attachment( $new_attachment,  $file_with_date, $post_id );

				$new_attachment_data = wp_generate_attachment_metadata( $new_attachment_id,   $file_with_date);

				wp_update_attachment_metadata( $new_attachment_id,  $new_attachment_data );

				global $wpdb;

				$get_post = $wpdb->get_row("SELECT post_content,post_title FROM $wpdb->posts WHERE ID = $post_id");

				$get_post_content = $get_post->post_content;

				$where = array( 'ID' => $post_id );

				$wpdb->update( $wpdb->posts, array( 'post_content'  =>  str_replace('pvtmed-uploads', 'uploads', $get_post_content)), $where );

			}
		}
	}

}

Private_Media_Helper_MJ::getInstance();