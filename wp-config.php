<?php
define('WP_MEMORY_LIMIT', '256M');
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

// PRODUCTION: www.msblogs.mattjennings.net
if($_SERVER['HTTP_HOST'] === 'www.msblogs.mattjennings.net' || $_SERVER['HTTP_HOST'] === 'www.msblogs.mattjennings.net'){
	/** The name of the database for WordPress */
	define('DB_NAME', 'msblogs_mattjennings_net');

	/** MySQL database username */
	define('DB_USER', 'msblogsmattjenni');

	/** MySQL database password */
	define('DB_PASSWORD', 'EKW!fyQa');

	/** MySQL hostname */
	define('DB_HOST', 'mysql.msblogs.mattjennings.net');
}
// LOCALHOST: local.msblogs.mattjennings.net
else {
	/** The name of the database for WordPress */
	define('DB_NAME', 'msblogs_mattjennings_net');

	/** MySQL database username */
	define('DB_USER', 'root');

	/** MySQL database password */
	define('DB_PASSWORD', 'root');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');

	define('WP_SITEURL', 'http://local.msblogs.mattjennings.net/');
	define('WP_HOME', 'http://local.msblogs.mattjennings.net/');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '20d9cpsdkFuunoaE1QZb!TFrSODQ(j""b?W6&liffafp|usbC`BZV^;ip`H/Q2:m');
define('SECURE_AUTH_KEY',  '(:TrcitYh7(/kwH4jQBp/OsM"ZsF*(k@e!1(`wG~kR/_Y"3+U@)A@VM@vq4%?KF|');
define('LOGGED_IN_KEY',    'ow6;@h!y1str@mBoylNoJ2mQNE3iH(+yx:hFOmrBdoP5#W4o~36$24Ock0tcqsRr');
define('NONCE_KEY',        'EqqiKE:Lv^xW)ZB~DJr&GbLif27O4sfV6tz~gMC1Wy1LE_V+Rjs:QTxoiDDYs#%v');
define('AUTH_SALT',        '|ZU2MJ:btNZzjLrf_yKD;TU@)r0B^jC"mH1LL_UZMjFS0lt5N!Q`ybUOSKS;bH4+');
define('SECURE_AUTH_SALT', 'Btq1V4I#?s)c_u_6&1aLr`/|*J(j)(VK|:Gmt2_JP0%s0g9v2$fk9C)oKPp/Y|9a');
define('LOGGED_IN_SALT',   ')|m!4sGH~*vz&qOV|R1:(Efb$QobV^jV_1)usiP9HL#oCqd"ka2J_qsjb`MH/9K4');
define('NONCE_SALT',       'N0d|:zzG#UZpc&3Aqa"V0R(6&jNpVEadv?|2NGfYWGRlK8Jq/Q|))Uqv65lOy@ID');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_46g7x3';

/**
 * Limits total Post Revisions saved per Post/Page.
 * Change or comment this line out if you would like to increase or remove the limit.
 */
define('WP_POST_REVISIONS',  10);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

